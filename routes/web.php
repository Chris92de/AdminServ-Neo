<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// homepage
if (!file_exists(base_path(".setupDone"))) {
    Route::get('/', "Setup@start");
} else {
    Route::get('/', "ServerController@index")->middleware(["auth", "locale"]);

    Route::get('/index', function () {
        return view('index');
    });
}

// first time setup
if (!file_exists(base_path(".setupDone"))) {
    Route::get('setup', "Setup@start");
    Route::post('setup', "Setup@postSetupMySql")->name("setup.mysql");

    Route::get('setup/1', "Setup@step1");

    Route::get('setup/2', "Setup@step2");
    Route::post('setup/2', "Setup@postSetupOauth")->name("setup.oauth");

    Route::post('setup/3', "Setup@postSetupUser")->name("setup.create");
    Route::get('setup/3', "Setup@step3");

    Route::get('setup/4', "Setup@step4");
}

// auth
Route::get('login/maniaplanet', 'Auth\\OauthLoginController@redirectToProvider');
Route::get('login/callback', 'Auth\\OauthLoginController@handleProviderCallback');
Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();


// all routes with authentication
Route::group(['middleware' => ['auth', 'locale']], function () {

    Route::resource('admin/users', 'UserController');

    Route::get("admin/users/{uid}/servers", 'UsersServerController@index')->middleware("role:adminserv-master")->middleware("checkserver:{sid}")->name("users.servers");
    Route::get("admin/users/{uid}/servers/assign", 'UsersServerController@assign')->middleware("role:adminserv-master")->name("users.server.assign");
    Route::patch("admin/users/{uid}/servers/assign", "UsersServerController@add")->middleware("role:adminserv-master")->name("userserver.add");
    Route::delete("admin/users/{uid}/servers/assign", "UsersServerController@remove")->middleware("role:adminserv-master")->name("userserver.remove");
    Route::get("admin/users/{uid}/servers/{sid}/assign-role", "UsersServerController@roles")->middleware("ability:adminserv-master,role-assign")->name("roles.assign");
    Route::patch("admin/users/{uid}/servers/{sid}/assign-role", "UserController@assignRole")->middleware("ability:adminserv-master,role-assign")->name("users.server.assignRole");

    Route::get('home', 'HomeController@index')->name('home');

    Route::get("server", "ServerController@index")->name("server.index");
    Route::get("server/create", "ServerController@create")->name("server.create");
    Route::post("server/create", "ServerController@store")->name("server.create");

    Route::get("server/{id}/overview", "ServerController@manage")->name("server.manage");
    Route::get("server/{id}/edit", "ServerController@edit")->middleware(['ability:adminserv-master,server-edit'])->name("server.edit");
    Route::patch("server/{id}/overview", "ServerController@update")->middleware(['ability:adminserv-master,server-edit'])->name("server.update");
    Route::post("server/{id}/delete", "ServerController@delete")->middleware(['ability:adminserv-master,server-edit'])->name("server.delete");

    Route::get('server/{sid}/roles', 'RoleController@index')->middleware(['ability:adminserv-master,role-list'])->name("roles.index");
    Route::get('server/{sid}/roles/create', 'RoleController@create')->middleware(['ability:adminserv-master,role-create'])->name("roles.create");
    Route::post('server/{sid}/roles/create', 'RoleController@store')->middleware(['ability:adminserv-master,role-create'])->name("roles.store");
    Route::get('server/{sid}/roles/{id}', 'RoleController@show')->name('roles.show');
    Route::get('server/{sid}/roles/{id}/edit', ['as' => 'roles.edit', 'uses' => 'RoleController@edit', 'middleware' => ['ability:adminserv-master,role-edit']]);
    Route::patch('server/{sid}/roles/{id}', ['as' => 'roles.update', 'uses' => 'RoleController@update', 'middleware' => ['ability:adminserv-master,role-edit']]);
    Route::delete('server/{sid}/roles/{id}', ['as' => 'roles.destroy', 'uses' => 'RoleController@destroy', 'middleware' => ['ability:adminserv-master,role-delete']]);

    Route::get('/profile', 'ProfileContoller@me')->name('profile.me');

    Route::get("server/{id}/options", "ServerSettingsController@serverOptions")->middleware(['ability:adminserv-master,server-options'])->name("server.options");
    Route::post("server/{id}/options", "ServerSettingsController@postServerOptions")->middleware(['ability:adminserv-master,server-options'])->name("server.options");
    Route::get("server/{id}/matchsettings", "ServerSettingsController@gameInfos")->middleware(['ability:adminserv-master,server-options'])->name("server.gameinfo");
    Route::post("server/{id}/matchsettings", "ServerSettingsController@postGameInfos")->middleware(['ability:adminserv-master,server-options'])->name("server.gameinfo");
    Route::get("server/{id}/scriptsettings", "ServerSettingsController@scriptSettings")->middleware(['ability:adminserv-master,server-options'])->name("server.scriptsettings");
    Route::post("server/{id}/scriptsettings", "ServerSettingsController@postScriptSettings")->middleware(['ability:adminserv-master,server-options'])->name("server.scriptsettings");

    Route::get("server/{id}/maps/upload", "ManiaExchangeController@index")->middleware(['ability:adminserv-master,server-options'])->name("server.maps.upload");
    Route::post("server/{id}/maps/upload", "ManiaExchangeController@postInstall")->middleware(['ability:adminserv-master,server-options'])->name("server.manage.mxinstall");
    Route::get("server/{id}/maps/browse", "FileBrowserController@index")->middleware(['ability:adminserv-master,maps-add'])->name("server.maps.filebrowser");

    Route::get("server/{id}/players", "ServerPlayerController@index")->middleware(['ability:adminserv-master,player-kick'])->name("server.manage.players");

    Route::get("/server/{id}/ajax/mapinfo", "Ajax\MapInfo@index");

    Route::get("/server/{id}/ajax/playerlist", "ServerPlayerController@getPlayerList")->name("ajax.getPlayers");
    Route::post("/server/{id}/ajax/playerlist/{login}", "ServerPlayerController@handleRequest")->name("ajax.handlePlayerListRequest");

    Route::get("/server/{id}/ajax/getChat", "Ajax\ChatController@getChatLines")->name("ajax.getchat");
    Route::post("/server/{id}/ajax/sendChat", "Ajax\ChatController@sendChatLine")->name("ajax.sendchat");

    Route::get("/server/{id}/ajax/maplist", "Ajax\MapInfo@maplist");
    Route::get("/server/{id}/ajax/mapupload", "Ajax\MapInfo@showUpload");
    Route::post("/server/{id}/ajax/mapupload", "Ajax\MapInfo@doUpload");

    Route::post("/server/{id}/ajax/mapinfo", "Ajax\MapInfo@update");
    Route::get("/server/{id}/ajax/serverinfo", "Ajax\ServerInfo@index");
    Route::post("/server/{id}/ajax/serverinfo", "Ajax\ServerInfo@update");
    Route::get('server/{id}/ajax/widget', "Ajax\ServerInfo@getAllInfo");

    Route::get("/server/{id}/ajax/quickcontrols", "Ajax\QuickControls@index");
    Route::get("server/{id}/ajax/quickcontrols/restartMap", "Ajax\QuickControls@restartMap")->name('server.quickcontrols.restartmap');
    Route::get("server/{id}/ajax/quickcontrols/skipMap", "Ajax\QuickControls@skipMap")->name('server.quickcontrols.skipmap');
    Route::get("server/{id}/ajax/quickcontrols/cancelVote", "Ajax\QuickControls@cancelVote")->name('server.quickcontrols.cancelvote');
    Route::get("server/{id}/ajax/quickcontrols/forceEndRound", "Ajax\QuickControls@forceEndRound")->name('server.quickcontrols.forceendround');

    Route::get("server/{id}/planets", "Planets@overview")->name("server.planets");
    Route::post("server/{id}/planets/transfer", "Planets@transfer")->name("server.planets.transfer");
    Route::post("server/{id}/planets/createBill", "Planets@billPlayer")->name("server.planets.createbill");


});
