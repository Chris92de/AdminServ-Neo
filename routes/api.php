<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/server/{id}/name', 'ApiController@getServerName');
Route::get('/server/{id}/status', 'ApiController@getServerStatus');
Route::get('/server/{id}/players', 'ApiController@getPlayerCount');
Route::get('/server/{id}/spectators', 'ApiController@getSpectatorCount');
Route::get('/server/{id}/currentmap', 'ApiController@getCurrentMap');
Route::get('/server/{id}/joinlink', 'ApiController@getJoinLink');
Route::get('/server/{id}/spectatelink', 'ApiController@getSpectatorLink');
