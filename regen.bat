echo off
php artisan config:clear
php artisan cache:clear
php artisan clear-compiled
composer dump-autoload
