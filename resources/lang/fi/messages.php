<?php

return [
    'welcome' => 'Tervetuloa!',
    'submit' => 'Lähtetä',
    'edit' => 'Muokkaa',
    'delete' => 'Poista',
];
