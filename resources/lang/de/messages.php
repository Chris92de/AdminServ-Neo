<?php

return [
    'welcome' => 'Willkommen bei #AdminServ!',
    // General messages that reappear everywhere
    'submit' => 'Absenden',
    'edit' => 'Bearbeiten',
    'delete' => 'Löschen',
    'create' => 'Erstellen',
    'cancel' => 'Abbrechen',
    'home' => 'Home',
    'servers' => 'Server',
    'users' => 'Benutzer',
    'manage' => 'Verwalten',
    'back' => 'Zurück',
    'logout' => 'Ausloggen',
    'login' => 'Login',
    'setting-name' => 'Name der Einstellung',
    'value' => 'Wert',
    'select-player' => 'Spieler auswählen',
    'transfer' => 'Übertragen',

    // Add/edit servers
    'add-new-server' => 'Neuen Server hinzufügen',
    'add-a-server' => 'Einen Server hinzufügen',
    'server-name' => 'Server Name',
    'server-host' => 'Host',
    'server-xml-rpc-port' => 'XML-RPC Port',
    'server-xml-rpc-user' => 'XML-RPC Benutzer (SuperAdmin)',
    'server-xml-rpc-pwd' => 'XML-RPC Passwort',
    'server-uses-ftp' => 'Server benutzt FTP zur Dateiübertragung (Nur aktivieren falls Host nicht localhost)',
    'server-uses-sftp' => 'Server benutzt SFTP',
    'server-ftp-remote-host' => 'Remote Host',
    'server-ftp-remote-port' => 'Remote Port',
    'server-ftp-remote-user' => 'Remote User',
    'server-ftp-remote-pass' => 'Remote Pass',
    // Server Manage Page and sub pages
    'server-manage-overview' => 'Übersicht',
    'server-manage-settings' => 'Einstellungen',
    'server-manage-game-settings' => 'Spieleinstellungen',
    'server-manage-server-options' => 'Serveroptionen',
    'server-manage-match-settings' => 'Matcheinstellungen',
    'server-manage-script-settings' => 'Skripteinstellungen',
    'server-manage-planets' => 'Planets',
    'server-manage-adminserv-specific' => 'AdminServ Spezifisch',
    'server-manage-roles-management' => 'Rollenverwaltung',
    'server-manage-planets-message' => '{0} Der Server hat momentan keine Planets.|{1} Der Server hat momentan :value Planet.|[2,*] Der Server hat momentan :value Planets.',
    'server-manage-planets-transfer' => 'Planets übertragen',
    'server-manage-planets-amount' => 'Planets Anzahl angeben',
    'server-manage-planets-send-bill' => 'Sende Rechnung an Spieler',
    'server-manage-planets-bill-msg' => 'Nachricht für die Rechnung angeben',
    'server-manage-planets-bill-create' => 'Rechnung erstellen'



];
