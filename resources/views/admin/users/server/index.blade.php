@extends('layouts.menuapp')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">{{$user->name}} {{__('messages.servers')}}</h2>
                </div>
                <div class="eight wide column right aligned">
                    @ability('adminserv-master','role-create')
                    <a class="ui green small labeled icon button" href="{{ route('users.server.assign', $uid) }}"><i
                                class="plus icon"></i>{{__('messages.user-server-assign')}}</a>
                    @endability
                </div>
            </div>
        </div>

        <table class="ui striped table">
            <thead>
            <tr class="center aligned">
                <th class="one wide">{{__('messages.number')}}</th>
                <th class="four wide">{{__('messages.name')}}</th>
                <th class="five wide">{{__('messages.description')}}</th>
                <th class="five wide">{{__('messages.action')}}</th>
            </tr>
            </thead>
            @foreach ($servers as $key => $server)
                <tr class="center aligned">
                    <td>{{ ++$i }}</td>
                    <td>{{ $server->name }}</td>
                    <td>{{ $server->description }}</td>
                    <td>
                        <a class="ui small green icon button" href="{{route('roles.assign', [$uid, $server->id])}}">
                            <i class="user add icon"></i>{{__('messages.user-server-role-assign')}}
                        </a>
                        @ability('adminserv-master','server-edit')
                        <a class="ui small teal icon button" href="{{ route('server.edit', [$server->id]) }}">
                            <i class="pencil icon"></i> Edit
                        </a>
                        @endability
                        @ability('adminserv-master','server-remove')
                        {!! Form::open(['method'=>'DELETE', 'route' => ['userserver.remove', $uid],'style'=>'display:inline']) !!}
                        <input type="hidden" name="serverId" value="{{$server->id}}"/>
                        <button class="ui small red icon button" type="button" onclick="doConfirm(this);" value="Delete">
                            <i class="remove icon"></i>{{__('messages.user-server-role-unassign')}}
                        </button>
                        {!! Form::close() !!}
                        @endability
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

    <div class="ui small confirm modal">
        <div class="ui icon header">
            <i class="delete icon"></i>
            {{__('messages.user-server-unassign-confirm')}}
        </div>
        <div class="actions">
            <div class="ui red cancel inverted button">
                <i class="remove icon"></i>
                {{__('messages.no')}}
            </div>
            <div class="ui green ok inverted button">
                <i class="checkmark icon"></i>
                {{__('messages.yes')}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        function doConfirm(target) {
            $('.ui.small.confirm.modal')
                .modal({
                    closable: false,
                    transition: "horizontal flip",
                    onDeny: function () {

                    },
                    onApprove: function () {
                        $(target).parent().submit();
                    }
                })
                .modal('show');
        }
    </script>
@endpush