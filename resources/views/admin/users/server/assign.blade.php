@extends('layouts.menuapp')

@section('title', 'Assign Server To User')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">{{__('messages.user-server-assignment')}}</h2>
                </div>
                <div class="eight wide column right aligned">
                    @ability('adminserv-master','role-create')
                    <a class="ui green small labeled icon button" href="{{ route('server.create') }}"><i
                                    class="plus icon"></i>{{__('messages.add-new-server')}}</a>
                    @endpermission
                </div>
            </div>
        </div>
        <table class="ui striped table">
            <thead>
            <tr class="center aligned">
                <th class="one wide">{{__('messages.number')}}</th>
                <th class="four wide">{{__('messages.name')}}</th>
                <th class="six wide">{{__('messages.description')}}</th>
                <th class="four wide">{{__('messages.action')}}</th>
            </tr>
            </thead>
            @foreach ($servers as $key => $server)
                <tr class="center aligned">
                    <td>{{ ++$i }}</td>
                    <td>{{ $server->name }}</td>
                    <td>{{ $server->description }}</td>
                    <td>
                        @ability('adminserv-master','server-add')
                        {!! Form::open(['method' => 'PATCH','route' => ['userserver.add', $uid],'style'=>'display:inline']) !!}
                        <input type="hidden" name="serverId" value="{{$server->id}}"/>
                        <button class="ui green small labeled icon button" type="submit" value="Submit"><i class="plus icon"></i>{{__('messages.add')}}</button>
                        {!! Form::close() !!}
                        @endability
                        <a class="ui small blue icon button" href="{{ route('server.manage', [$server->id]) }}"><i class="options icon"></i>{{__('messages.manage')}}</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
