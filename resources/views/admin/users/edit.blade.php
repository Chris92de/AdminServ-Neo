@extends('layouts.menuapp')

@section('title', 'Editing User')

@section('content')
    <div class="ui very padded raised container segment">

        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">{{__('messages.user-edit')}}</h2>
                </div>
                <div class="eight wide column right aligned">
                    <a class="ui black labeled icon button" href="{{ route('users.index') }}"><i
                                class="reply icon"></i><span>{{ __('messages.back') }}</span></a>
                </div>
            </div>
        </div>

        {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id], "class" => "ui form"]) !!}
        <div class="required field {{ $errors->first("login", "error") }}">
            <label class="label">{{__('messages.user-mp-login')}}</label>
            {!! Form::text('login', null, array('placeholder' => 'Maniaplanet login', "class"=> "form-control")) !!}
            {!!  $errors->first("login", "<div class='ui pointing red basic label'>:message</div>")  !!}
        </div>
        <div class="two fields">
            <div class="required field {{ $errors->first("name", "error") }}">
                <label class="label">{{__('messages.name')}}</label>
                {!! Form::text('name', null, array('placeholder' => 'Name', "class"=> "input")) !!}
                {!! $errors->first("name", "<div class='ui pointing red basic label'>:message</div>") !!}
            </div>

            <div class="field">
                <label class="label">{{__('messages.user-email')}}</label>
                <p class="control">
                    {!! Form::text('email', null, array('placeholder' => 'Valid and working email', "class" => "input")) !!}
                </p>
                <p class="help is-danger">{{ $errors->first("email") }}</p>
            </div>
        </div>

        <div class="two fields">
            <div class="field">
                <label class="label">{{__('messages.user-password')}}</label>
                <p class="control">
                    {!! Form::password('password', ['placeholder' => 'Password', "class" => "input"]) !!}
                </p>
                <p class="help is-danger">{{ $errors->first("password") }}</p>
            </div>
            <div class="field">
                <label class="label">{{__('messages.user-password-confirm')}}</label>
                <p class="control">
                    {!! Form::password('confirm-password', ['placeholder' => 'Confirm Password', "class" => "input"]) !!}
                </p>
                <p class="help is-danger">{{ $errors->first("confirm-password") }}</p>
            </div>
        </div>

        <div class="two fields">
            <div class="field">
                <label class="label">{{__('messages.user-ui-lang')}}</label>
                <p class="select">
                    {!! Form::select('locale', $lang, $lang) !!}
                </p>
            </div>

            <div class="field">
                <label class="label">{{__('messages.role')}}</label>
                <p class="select">
                    {!! Form::select('roles[]', $roles,[], array('class' => 'select')) !!}
                </p>
            </div>
        </div>

        <div class="field">
            <button type="submit" class="ui green icon labeled button"><i class="checkmark icon"></i>{{__('messages.save-changes')}}
            </button>
            <a class="ui   button" href="{{ route('users.index') }}"><span>{{__('messages.cancel')}}</span></a>
        </div>

    {!! Form::close() !!}
    </div>
@endsection
