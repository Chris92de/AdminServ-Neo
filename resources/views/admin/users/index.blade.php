@extends ('layouts.menuapp')

@section('title', 'User Management')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">{{__('messages.user-management')}}</h2>
                </div>
                <div class="eight wide column right aligned">
                    @ability('adminserv-master','global-user-create')
                    <a class="ui green small labeled icon button" href="{{ route('users.create') }}"><i
                                class="plus icon"></i> {{__('messages.user-create')}}</a>
                    @endability
                </div>
            </div>
        </div>

        <table class="ui striped table">
            <thead>
            <tr class="center aligned">
                <th class="one wide">{{__('messages.number')}}</th>
                <th class="four wide">{{__('messages.name')}}</th>
                <th class="four wide">{{__('messages.roles')}}</th>
                <th class="one wide">{{__('messages.servers')}}</th>
                <th class="five wide">{{__('messages.action')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key => $user)
                <tr class="center aligned">
                    <td>{{ ++$i }}</td>
                    <td>{{ $user->name }}</td>
                    <td>
                        @if(!empty($user->roles))
                            @foreach($user->roles as $v)
                                <label class="label label-success">{{ $v->display_name }}</label>
                            @endforeach
                        @endif
                    </td>
                    <td>
                        @if(!empty($user->servers))
                            {{ count($user->servers) }}
                        @else
                            -
                        @endif
                    </td>
                    <td>
                        <!--<a class="ui small blue icon button" href="{{ route('users.show',$user->id) }}">Show</a>-->
                        @role('adminserv-master')
                        <a class="ui small grey icon button" href="{{ route('users.servers',$user->id) }}"><i class="cloud icon"></i> {{__('messages.servers')}}</a>
                        <a class="ui small teal icon button" href="{{ route('users.edit',$user->id) }}"><i class="pencil icon"></i> {{__('messages.edit')}}</a>
                        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                        <button class="ui small red icon button" type="button" oncLick="doConfirm(this);"
                                value="Delete"><i
                                    class="remove icon"></i> {{__('messages.delete')}}
                        </button>
                        {!! Form::close() !!}
                        @endrole
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $data->render() !!}
    </div>

    <div class="ui small confirm modal">
        <div class="ui icon header">
            <i class="delete icon"></i>
            {{__('messages.user-delete-confirm')}}
        </div>
        <div class="actions">
            <div class="ui red cancel inverted button">
                <i class="remove icon"></i>
                {{__('messages.no')}}
            </div>
            <div class="ui green ok inverted button">
                <i class="checkmark icon"></i>
                {{__('messages.yes')}}
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    function doConfirm(target) {
        $('.ui.confirm.modal')
            .modal({
                closable: false,
                transition: "horizontal flip",
                onDeny: function () {

                },
                onApprove: function () {
                    $(target).parent().submit();
                }
            })
            .modal('show');
    }
</script>
@endpush
