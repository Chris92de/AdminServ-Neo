@extends('layouts.app')

@push('style')
<style>
    html, body {
        background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url('/images/background.jpg');
        background-position: center center;
    }

    body > .grid {
        height: 100%;
    }

    .image {
        margin-top: -100px;
    }

    .column {
        max-width: 450px;
    }
</style>
@endpush
@section('content')
    <div class="ui middle aligned center aligned grid ">
        <div class="column">

            <div class="ui image header">
                <div class="content animated flipInY">
                    <img src="/images/logo2.png"/>
                </div>
            </div>

            <form class="ui large form" method="POST" action="{{ route('login') }}">
                <div class="ui stacked animated fadeIn segment">

                    {{ csrf_field() }}

                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" placeholder="{{__('messages.user-email')}}"
                                   value="{{ old('email') }}" required
                                   autofocus/>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="{{__('messages.user-password')}}" required/>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui toggle checkbox">
                            <input type="checkbox" class="hidden"
                                   name="remember" {{ old('remember') ? 'checked' : '' }} />
                            <label for="remember">{{__('messages.user-login-remember')}}</label>
                        </div>
                    </div>

                    <div class="ui message">
                        <div class="ui buttons">
                            <button type="submit" class="ui button">{{__('messages.login')}}</button>
                            <div class="or"></div>
                            <a class="ui button maniaconnect" href="/login/maniaplanet">{{__('messages.user-login-maniaconnect')}}</a>
                        </div>
                    </div>

                </div>
            </form>
            <a class="ui link" href="{{ route('password.request') }}">
                {{__('messages.user-forgot-password')}}
            </a>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $('.ui.checkbox').checkbox();
</script>
@endpush
