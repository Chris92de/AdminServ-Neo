@extends('layouts.menuapp')
@section('title', 'Home')
@section('content')
    <div class="ui very raised container segment">
        <h1 class="ui center aligned header">
            {{ __('messages.welcome') }}
        </h1>
        <div class="ui grid">
            <div class="nine wide left floated left aligned column">
                <h4 class="ui header"><i class="server icon"></i><div class="content">Server Overview</div></h4>
                @foreach($servers as $key => $server)
                    <div class="row">
                        <div class="ui raised card" data-ajax-url="/server/{{$server->id}}/ajax/widget">
                            <!--<div class="content">
                                <div class="center aligned header" data-ajax-json="/api/server/{{$server->id}}/name" data-ajax-field="name"></div>
                                <div class="meta" data-ajax-json="/api/server/{{$server->id}}/status" data-ajax-field="status">Status: </div>
                                <div class="description">
                                    <div data-ajax-json="/api/server/{{$server->id}}/players" data-ajax-field="count">
                                        <i class="user outline icon"></i> Players:
                                    </div>
                                    <div data-ajax-json="/api/server/{{$server->id}}/spectators" data-ajax-field="count">
                                        <i class="eye icon"></i> Spectators:
                                    </div>
                                    <div data-ajax-json="/api/server/{{$server->id}}/currentmap" data-ajax-field="map">
                                        <i class="map icon"></i> Map:
                                    </div>
                                </div>
                            </div>
                            <div class="ui three bottom attached buttons">
                                <a data-ajax-button="/api/server/{{$server->id}}/joinlink" class="ui button">
                                    <i class="play icon"></i>
                                    Join
                                </a>
                                <a data-ajax-button="/api/server/{{$server->id}}/spectatelink" class="ui button">
                                    <i class="eye icon"></i>
                                    Spectate
                                </a>
                                <a href="{{route('server.manage', $server->id)}}" class="ui button">
                                    <i class="settings icon"></i>
                                    Manage
                                </a>
                            </div>-->
                        </div>
                    </div>
                    <br>
                @endforeach
            </div>
            <div class="five wide right floated right aligned column">
                <div class="row">
                    <h4 class="ui header"><i class="twitter icon"></i><div class="content">Latest Updates</div></h4>
                    <a class="twitter-timeline" data-width="360" data-height="600" data-dnt="true" href="https://twitter.com/AdminServ?ref_src=twsrc%5Etfw">Tweets by AdminServ</a>
                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
        </div>
    </div>
@endsection