<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminServ-Neo - @yield('title')</title>
    <meta name="description"
          content="AdminServ is an easy-to-use web interface to control your ManiaPlanet servers.">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/semantic/semantic.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/toastr.css') }}"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    @stack('header')
</head>
<body class="ui dimmable">
<div class="ui main menu fixed top pointing inverted borderless">
    <div class="ui container">
        <div class="header item horizontally vertically fitted">
            <a href="#"><img src="{{ URL::asset('/images/logo-dropshadow.png') }}" height="30px"></a>
        </div>
        <a class="item" href="{{route("home")}}">Home</a>
        @if (Auth::check())
            <a class="item" href="{{route("server.index")}}">{{__('messages.servers')}}</a>
            @role('adminserv-master')
            <a class="item" href="{{route("users.index")}}">{{__('messages.users')}}</a>
            @endrole
        @endif
        <div class="menu right">
            @if(Auth::check())
                <a class="item" href="#"><i class="white user icon"></i>
                    @if (Maniaplanet::toHtml(Auth::user()->nickname) != '')
                    <span class="maniaplanet">{!! Maniaplanet::toHtml(Auth::user()->nickname) !!}</span>
                    @else <span>{!! Auth::user()->name !!}</span>
                    @endif
                </a>
                <a class="item" href="/logout"><i class="white sign out icon"></i>{{__('messages.logout')}}</a>
            @else
                <a class="item floated" href="/login"><i class="white sign in icon"></i>{{__('messages.login')}}</a>
            @endif
        </div>
    </div>
</div>


@if($errors->any())
    @foreach($errors->all() as $error)
        <!--    <script>toastr["error"]('{{ $error }}');</script> -->
    @endforeach
@endif

@yield('content')

<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="{{ URL::asset('js/jquery-sortable-min.js') }}"></script>
<script src="{{ URL::asset('semantic/semantic.min.js') }}"></script>
<script src="{{ URL::asset('js/toastr.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.jeditable.js') }}"></script>
<script src="{{ URL::asset('js/mp-style-parser.js') }}"></script>
<script>
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "2500",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    $('.ui.dropdown')
        .dropdown()
    ;
</script>

@if( session()->has('info'))
    <script>toastr["info"]('{{ session()->get('info')}}');</script>
@endif
@if(session()->has('success'))
    <script>toastr["success"]('{{ session()->get('success')}}');</script>
@endif
@if(session()->has('warning'))
    <script>toastr["warning"]('{{ session()->get('warning')}}');</script>
@endif
@if(session()->has('error'))
    <script>toastr["error"]('{{ session()->get('error')}}');</script>
@endif


<script>
    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{!!  csrf_token() !!}"
            }
        });

        $('.ui.checkbox').checkbox();

        $('div[data-ajax-update]').each(function () {
            updater(this);
        });

        $('div[data-ajax-url]').each(function () {
            var elem = this;
            $(elem).prepend('<div class="ui text active loader">Updating</div>');
            $.ajax({
                url: $(elem).data("ajaxUrl"),

                success: function (data) {
                    $(elem).html(data);
                },
                complete: function () {
                    inplaceEdit();
                    parse();
                },
                error: function (xhr) {
                    $(elem).html("Ajax error " + xhr.status + ":" + xhr.statusText);
                }
            });
        });

        // do message
        $('.message .close')
            .on('click', function () {
                    $(this)
                        .closest('.message')
                        .transition('fade');
                }
            );
    });

    function updater(elem) {
        $(elem).prepend('<div class="ui text active loader">Updating</div>');
        $.ajax({
            url: $(elem).data("ajaxUpdate"),
            success: function (data) {
                $(elem).html(data);
            },
            complete: function () {
                inplaceEdit();
                parse();
                setTimeout(function () {
                    updater(elem)
                }, 30000);

            },
            error: function (xhr) {
                $(elem).html("Ajax error " + xhr.status + ":" + xhr.statusText);
            }
        });
    }

    function inplaceEdit() {
        $('span[data-editable]').each(function () {
            $(this).addClass("inplaceEdit");
            $(this).editable($(this).data("editable"), {
                indicator: "<div class='ui active inline loader'></div>",
                tooltip: "Doubleclick to edit...",
                event: "dblclick",
                method: "POST",
                data: $(this).data("string"),
                callback: (function () {
                    parse();
                })
            });
        });
    }

    function parse() {
        $('.ui.checkbox').checkbox();
        $('*[data-maniaplanet]').each(function () {
            // console.log("found");
            var html = $(this).html();
            $(this).html(MPStyle.Parser.toHTML(html));
        });
    }

</script>

@stack('scripts')

{{  \DebugBar::enable() }}
</body>
