@extends('layouts.app')

@section('title', 'Setup Wizard')

@push('style')
<style>
    .limited {
        max-width: 450px;
        margin: 0 auto;
    }
</style>
@endpush
@section('content')
    <div class="ui container">
        <img src="{{ URL::asset('/images/logo-dropshadow.png') }}" class="ui centered image"/>

        <div class="ui tiny ordered four steps">
            <div class="done step">
                <div class="content">
                    <div class="title">Host setup</div>
                    <div class="description">Setup Host location and Mysql databases</div>
                </div>
            </div>
            <div class="done step">
                <div class="content">
                    <div class="title">Maniaplanet integration</div>
                    <div class="description">Setup Oauth2</div>
                </div>
            </div>
            <div class="done step">
                <div class="content">
                    <div class="title">Create user</div>
                    <div class="description">Create master admin</div>
                </div>
            </div>
            <div class="active step">
                <div class="content">
                    <div class="title">Done</div>
                    <div class="description">Let's start</div>
                </div>
            </div>
        </div>
        <br/>
        <br/>
        <div class="limited">
            <div class="ui center aligned segment animated tada">
                <h2 class="ui header">All Done</h2>
                <div class="ui animated huge green inverted fade button">
                    <div class="visible content">Finish</div>
                    <div class="hidden content">
                        <a style="color: white;" href="/">Let's Go!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

