@extends('layouts.app')

@section('content')

    <div class="ui container">
        <img src="{{ URL::asset('/images/logo-dropshadow.png') }}" class="ui centered image"/>

        <div class="ui tiny ordered four steps">
            <div class="done step">
                <div class="content">
                    <div class="title">Host setup</div>
                    <div class="description">Setup Host location and Mysql databases</div>
                </div>
            </div>
            <div class="done step">
                <div class="content">
                    <div class="title">Maniaplanet integration</div>
                    <div class="description">Setup Oauth2</div>
                </div>
            </div>
            <div class="active step">
                <div class="content">
                    <div class="title">Create user</div>
                    <div class="description">Create master admin</div>
                </div>
            </div>
            <div class="step">
                <div class="content">
                    <div class="title">Done</div>
                    <div class="description">Let's start</div>
                </div>
            </div>
        </div>
        <div class="ui segment">
            <h3 class="ui dividing header">Adminserv User creation</h3>

            {!! Form::open(array('route' => 'setup.create','method'=>'POST', "class" => "ui form")) !!}

            <div class="required field {{ $errors->first("name", "error") }}">
                <label class="label">Name</label>
                {!! Form::text('name', null, array('placeholder' => 'Name')) !!}
                {!! $errors->first("name", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>

            <div class="required field {{ $errors->first("login", "error") }}">
                <label class="label">Maniaplanet login</label>
                {!! Form::text('login', null, array('placeholder' => 'Your Maniaplanet login')) !!}
                {!! $errors->first("login", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>

            <div class="required field {{ $errors->first("email", "error") }}">
                <label class="label">Email</label>
                {!! Form::text('email', null, array('placeholder' => 'Valid and working email')) !!}
                {!! $errors->first("email", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>

            <div class="required field {{ $errors->first("password", "error") }}">
                <label class="label">Password</label>
                {!! Form::password('password', array('placeholder' => 'Password')) !!}
                {!! $errors->first("password", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>

            <div class="required field {{ $errors->first("confirm-password", "error") }}">
                <label class="label">Confirm Password</label>
                {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password')) !!}
                {!! $errors->first("confirm-password", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>

            <div class="required field">
                <label class="label">Role</label>
                {!! Form::select('roles[]', $roles, [], array('class' => 'multiple')) !!}
            </div>

            <div class="field">
                <button type="submit" class="ui green button">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
