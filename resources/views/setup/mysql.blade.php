@extends('layouts.app')

@section('title', 'Setup Wizard')

@section('content')

    <div class="ui container">
        <img src="{{ URL::asset('/images/logo-dropshadow.png') }}" class="ui centered image"/>

        <div class="ui tiny ordered four steps">
            <div class="active step">
                <div class="content">
                    <div class="title">Host setup</div>
                    <div class="description">Setup Host location and Mysql databases</div>
                </div>
            </div>
            <div class="step">
                <div class="content">
                    <div class="title">Maniaplanet integration</div>
                    <div class="description">Setup Oauth2</div>
                </div>
            </div>
            <div class="step">
                <div class="content">
                    <div class="title">Create user</div>
                    <div class="description">Create master admin</div>
                </div>
            </div>
            <div class="step">
                <div class="content">
                    <div class="title">Done</div>
                    <div class="description">Let's start</div>
                </div>
            </div>
        </div>
        <div class="ui segment">
            {!! Form::open(['url' => '/setup', "class" => 'ui form']) !!}
            {!! Form::token() !!}

            <h3 class="ui dividing header">Host Information</h3>

            <div class=" required field {{ $errors->first("appurl", "error") }}">
                <label>Public Ip or Hostname for this site</label>
                {!! Form::text('appurl', "", ['class' => 'input', "placeholder" =>  "Should start with http://"]) !!}
                {!!  $errors->first("appurl", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>

            <h3 class="ui dividing header">Database Information</h3>

            <div class="required field {{ $errors->first("host", "error") }}">
                <label class="label">Database host</label>
                {!! Form::text('host', "", ['class' => 'input', "placeholder" =>  "Default: 127.0.0.1 or localhost"]) !!}
                {!!  $errors->first("host", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>
            <div class="required field {{ $errors->first("port", "error") }}">
                <label class="label">Database port</label>
                {!! Form::text('port', "", ['class' => 'input', "placeholder" =>  "Default: 3306"]) !!}
                {!!  $errors->first("port", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>
            <div class="required field {{ $errors->first("database", "error") }}">
                <label class="label">Database name</label>
                <input name="database" class="input" type="text" placeholder="Enter the name of the database to use">
                {!!  $errors->first("database", "<div class='ui pointing red basic label'>:message</div>")  !!}

            </div>
            <div class="required field {{ $errors->first("user")  ? "error":"" }}">
                <label class="label">Database username</label>
                <p class="control">
                    <input name="user" class="input" type="text"
                           placeholder="Enter the user to connect to the database with">
                </p>
                {!!  $errors->first("user", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>
            <div class="required field {{ $errors->first("pass")  ? "error":"" }}">
                <label class="label">Database user password</label>
                <p class="control">
                    <input name="pass" class="input" type="password" placeholder="Enter the user's password">
                </p>
                {!!  $errors->first("pass", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>
            <div class="field">
                <button type="submit" class="ui button">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

