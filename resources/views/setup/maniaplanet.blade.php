@extends('layouts.app')

@section('title', 'Setup Wizard')

@section('content')

    <div class="ui container">
        <img src="{{ URL::asset('/images/logo-dropshadow.png') }}" class="ui centered image"/>

        <div class="ui tiny ordered four steps">
            <div class="done step">
                <div class="content">
                    <div class="title">Host setup</div>
                    <div class="description">Setup Host location and Mysql databases</div>
                </div>
            </div>
            <div class="active step">
                <div class="content">
                    <div class="title">Maniaplanet integration</div>
                    <div class="description">Setup Oauth2</div>
                </div>
            </div>
            <div class="step">
                <div class="content">
                    <div class="title">Create user</div>
                    <div class="description">Create master admin</div>
                </div>
            </div>
            <div class="step">
                <div class="content">
                    <div class="title">Done</div>
                    <div class="description">Let's start</div>
                </div>
            </div>
        </div>
        <div class="ui segment">
            <h4 class="ui dividing header">Maniaplanet oauth2 integration</h4>
            <p>
                Create a new maniaconnect app: <a href="https://v4.live.maniaplanet.com/web-services-manager/create"
                                                  target="_blank">https://v4.live.maniaplanet.com/web-services-manager/create</a><br>
                For redirect URI use following address: <code>{{ config('app.url')."/login/callback" }}</code>
                <br>
            </p>
            <br>

            <h4 class="ui dividing header">Details</h4>
            {!! Form::open(['url' => '/setup/2', "class" => "ui form"]) !!}

            {!! Form::token() !!}

            <div class="field">
                <div class="ui toggle checkbox">
                    <input tabindex="0" class="hidden" value="true" type="checkbox">
                    <label>Use Maniaplanet Oauth2</label>
                </div>
            </div>

            <div class="two fields">
                <div class="field">
                    <label class="label">Client ID</label>
                    <input name="client_id" class="input" type="text"
                           placeholder="Client ID from maniaplanet webservices">
                </div>
                <div class="field">
                    <label class="label">Client Secret</label>
                    <input name="client_secret" class="input" type="text"
                           placeholder="Client Secret from maniaplanet webservices">
                </div>
            </div>

            <div class="field">
                <button type="submit" class="button is-primary">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $('.ui.checkbox').checkbox();
</script>
@endpush
