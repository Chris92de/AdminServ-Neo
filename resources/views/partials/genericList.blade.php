<table class="ui striped table">
    <thead>
    <tr>
        <th class="right aligned four wide">Setting name</th>
        <th class="twelwe wide">Value</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key => $value)
        @if(is_bool($value))
            <tr>
                <td class="right aligned">
                    <label>{{$key}}</label>
                </td>
                <td>
                    <div class="inline field {{ $errors->first($key, "error") }}">
                        <div class="ui toggle checkbox">
                            <input tabindex="0" class="hidden" name="{{$key}}"
                                   {{ $value?"checked":"" }} type="checkbox">
                        </div>
                    </div>
                    {!!  $errors->first($key, "<div class='ui pointing red basic label'>:message</div>")  !!}
                </td>
            </tr>
        @endif
        @if(is_string($value))
            <tr>
                <td class="right aligned">
                    <label>{{$key}}</label>
                </td>
                <td>
                    <div class="inline field {{ $errors->first($key, "error") }}">
                        <input tabindex="0" class="text" name="{{$key}}" value="{{$value}}" type="text">
                        {!!  $errors->first($key, "<div class='ui pointing red basic label'>:message</div>")  !!}
                    </div>
                </td>
            </tr>
        @endif
        @if(is_int($value))
            <tr>
                <td class="right aligned">
                    <label>{{$key}}</label>
                </td>
                <td>
                    <div class="inline field {{ $errors->first($key, "error") }}">

                        <input tabindex="0" class="number" name="{{$key}}" value="{{$value}}" min="-1" type="number"
                               step="1">
                        {!!  $errors->first($key, "<div class='ui pointing red basic label'>:message</div>")  !!}
                    </div>
                </td>
            </tr>
        @endif
        @if(is_float($value))
            <tr>
                <td class="right aligned">
                    <label>{{$key}}</label>
                </td>
                <td>
                    <div class="inline field {{ $errors->first($key, "error") }}">
                        <input tabindex="0" class="number" name="{{$key}}" value="{{$value}}" min="-1" type="number"
                               step="0.1">
                        {!!  $errors->first($key, "<div class='ui pointing red basic label'>:message</div>")  !!}
                    </div>
                </td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>
