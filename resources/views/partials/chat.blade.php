<div class="ui basic section">
    <div id="chat">

    </div>
    <div class="ui action fluid input">
        <input required id="chatline" type="text" name="chatline"/>
        <div class="ui button" id="send">Send</div>
    </div>
</div>
<style>
    #chat {
        background: #222;
        color: #ff0;
        height: 400px;
        overflow-y: scroll;
        overflow-x: hidden;
    }

</style>
@push('scripts')
<script>

    $("#send").click(function () {
        sendChat();
    });

    $("#chatline").submit(function () {
        sendChat();
    });

    function sendChat() {
        $.ajax({
            type: "POST",
            url: "{{  route("ajax.sendchat", $id) }}",
            data: {
                chat: $('#chatline').val()
            },
            success: function () {

            }
        });
        $('#chatline').val("");
    }

    $(function () {
        chat_updater([]);
    });


    function chat_updater(oldBuffer) {
        $.ajax({
            url: "{{ route("ajax.getchat", $id) }}",
            success: function (data) {

                var difference = [];

                $.grep(data, function (el) {
                    if ($.inArray(el, oldBuffer) == -1) {
                        $('#chat').append("<div class='maniaplanet'>" + MPStyle.Parser.toHTML(el) + "</div>");
                        $('#chat').scrollTop($('#chat')[0].scrollHeight);
                    }
                });

                difference = null;
                setTimeout(function () {
                    chat_updater(data);
                }, 5000);
            },
            complete: function () {

            },
            error: function (xhr) {
                console.log("error");
            }
        });
    }
</script>
@endpush
