<?php use \App\Http\Controllers\Ajax\QuickControls; ?>

@extends('layouts.menuapp')

@section('title', 'Server Management')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="sixteen wide column">
                    <div class="ui menu">
                        <a class="active item" href="{{ route('server.manage', [$id]) }}">
                            Overview
                        </a>
                        <div class="ui dropdown link item">
                            <i class="setting icon"></i> Settings<i class="dropdown icon"></i>
                            <div class="menu">
                                <div class="header">Game Settings</div>
                                @ability('adminserv-master', 'server-options,server-password')
                                <a class="item" href="{{ route('server.options', [$id]) }}"><i class="options icon"></i> Server Options</a>
                                @endability
                                @ability('adminserv-master', 'game-control')
                                <a class="item" href="{{ route('server.gameinfo', [$id]) }}"><i class="trophy icon"></i> Match Settings</a>
                                <a class="item" href="{{ route('server.scriptsettings', [$id]) }}"><i class="code icon"></i> Script Settings</a>
                                @endability
                                @ability('adminserv-master', 'planets-view,planets-send')
                                <a class="item" href="{{ route('server.planets', [$id]) }}"><i class="money icon"></i> Planets</a>
                                @endability
                                @ability('adminserv-master', 'role-list,role-create,role-edit,role-delete')
                                <div class="header">AdminServ specific</div>
                                <a class="item" href="{{ route('roles.index', [$id]) }}"><i class="law icon"></i> Roles Management</a>
                                @endability
                            </div>
                        </div>
                        <div class="ui dropdown link item">
                            <i class="map icon"></i> Maps<i class="dropdown icon"></i>
                            <div class="menu">
                                <a class="item" href="{{ route('server.maps.upload', [$id]) }}"><i class="map icon"></i> Upload Maps & MX</a>
                                <a class="item" href="{{ route('server.maps.filebrowser', [$id]) }}"><i class="folder open icon"></i> File Browser</a>
                            </div>
                        </div>
                        <a class="item" href="{{ route('server.manage.players', [$id]) }}"><i class="gamepad icon"></i> Players</a>
                        <div class="right menu" data-ajax-url="/server/{{$id}}/ajax/quickcontrols">
                        </div>
                    </div>
                    <div class="ui segment">
                        <div class="ui divided grid">
                            <div class="row">
                                <div class="eight wide column">
                                    <div class="ui green ribbon label">Server Status</div>
                                    <div class="center aligned basic ui segment">
                                        <div class="ajax" data-ajax-update="/server/{{$id}}/ajax/serverinfo"></div>
                                    </div>
                                </div>
                                <div class="eight wide column">
                                    <div class="ui ajax">
                                        <div data-ajax-update="/server/{{$id}}/ajax/mapinfo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui segment">
            <div class="ui grid">
                <div class="row">
                    <div class="sixteen wide column">
                        <div class="ui green ribbon label">Chat</div>
                        <br><br>
                            @include("partials.chat", ["id" => $id])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
