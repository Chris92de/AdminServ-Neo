@extends('layouts.menuapp')

@section('title', 'Server Management')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">Server management</h2>
                </div>
                <div class="eight wide column right aligned">
                    @ability('adminserv-master','role-create')
                    <a class="ui green small labeled icon button" href="{{ route('server.create') }}"><i
                                class="plus icon"></i>Add New Server</a>
                    @endability
                </div>
            </div>
        </div>

        <table class="ui striped table">
            <thead>
            <tr class="center aligned">
                <th class="one wide">No</th>
                <th class="four wide">Name</th>
                <th class="five wide">Description</th>
                <th class="five wide">Action</th>
            </tr>
            </thead>
            @foreach ($servers as $key => $server)
                <tr class="center aligned">
                    <td>{{ ++$i }}</td>
                    <td>{{ $server->name }}</td>
                    <td>{{ $server->description }}</td>
                    <td>
                        <a class="ui small blue icon button" href="{{ route('server.manage', [$server->id]) }}"><i
                                    class="options icon"></i> Manage</a>
                        @ability('adminserv-master','server-edit')
                        <a class="ui small teal icon button" href="{{ route('server.edit', [$server->id]) }}"><i
                                    class="pencil icon"></i> Edit</a>

                        @endability
                        @ability('adminserv-master','server-delete')
                        {!! Form::open(['route' => ['server.delete', $server->id],'style'=>'display:inline']) !!}
                        <button class="ui small red icon button" type="button" onclick="doConfirm(this);" value="Delete"><i class="remove icon"></i> Delete
                        </button>
                        {!! Form::close() !!}
                        @endability
                    </td>
                </tr>
            @endforeach
        </table>
        {{ $servers->links() }}
    </div>
    <div class="ui small  test modal">
        <div class="ui icon header">
            <i class="delete icon"></i>
            Do you really want to delete this server?
        </div>
        <div class="actions">
            <div class="ui red cancel inverted button">
                <i class="remove icon"></i>
                No
            </div>
            <div class="ui green ok inverted button">
                <i class="checkmark icon"></i>
                Yes
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    function doConfirm(target) {
        $('.ui.test.modal')
            .modal({
                closable: false,
                transition: "horizontal flip",
                onDeny: function () {

                },
                onApprove: function () {
                    $(target).parent().submit();
                }
            })
            .modal('show');
    }
</script>
@endpush
