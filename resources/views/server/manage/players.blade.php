<?php
/** @var \Maniaplanet\DedicatedServer\Structures\Player $player */
?>

@extends("layouts.menuapp")

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">Manage Players</h2>
                </div>
                <div class="eight wide column right aligned">
                    <a class="ui black labeled icon button" href="{{ route('server.manage', [$id]) }}"><i class="reply icon"></i><span>Back</span></a>
                </div>
            </div>
            <div class="row">
                <div class="sixteen wide center aligned column">
                    <div data-ajax-url="/server/{{$id}}/ajax/playerlist">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection