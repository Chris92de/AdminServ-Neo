@extends('layouts.menuapp')

@section('title', 'Assign Role to User')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">Assign Role To User</h2>
                </div>
                <div class="eight wide column right aligned">
                    <a class="ui black labeled icon button" href="{{ route('users.servers', $uid) }}">
                        <i class="reply icon"></i>
                        Back
                    </a>
                </div>
            </div>
        </div>
        <table class="ui striped table">
            <thead>
            <tr class="center aligned">
                <th class="one wide">No</th>
                <th class="two wide">Name</th>
                <th class="five wide">Description</th>
                <th class="five wide">Action</th>
            </tr>
            </thead>

            @foreach($roles as $key => $role)
            <tr class="center aligned">
                <td>{{++$i}}</td>
                <td>{{$role->display_name}}</td>
                <td>{{$role->description}}</td>
                <td>
                    {!! Form::open(['method' => 'PATCH', 'route' => ['users.server.assignRole', $sid, $uid], 'style' => 'display:inline']) !!}
                    <input type="hidden" name="role" value="{{$role->id}}">
                    <button class="ui small green icon button" type="submit">
                        <i class="add user icon"></i>
                        Assign To User
                    </button>
                </td>
            </tr>
            @endforeach
            {!! Form::close() !!}
        </table>
    </div>
@endsection