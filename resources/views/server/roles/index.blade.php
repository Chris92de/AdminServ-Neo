@extends('layouts.menuapp')

@section('title', 'Role Management')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="subtitle">Role Management for Server ID {{$sid}}</h2>
                </div>
                <div class="eight wide column right aligned">
                    @ability('adminserv-master', 'role-create')
                    <a class="ui green small labeled icon button" href="{{ route('roles.create', $sid) }}"><i class="plus icon"></i> Add New Role</a>
                    @endability
                    <a class="ui black small labeled icon button" href="{{ route('server.manage', $sid) }}"><i class="reply icon"></i> Back</a>
                </div>
            </div>
        </div>
        <table class="ui striped table">
            <thead>
                <tr class="center aligned">
                    <th class="one wide">No</th>
                    <th class="two wide">Name</th>
                    <th class="five wide">Description</th>
                    <th class="five wide">Action</th>
                </tr>
            </thead>
            @foreach ($roles as $key => $role)
            <tr class="center aligned">
                <td>{{++$i}}</td>
                <td>{{$role->display_name}}</td>
                <td>{{$role->description}}</td>
                <td>
                    <a class="ui small blue icon button" href="{{ route('roles.show', [$sid, $role->id]) }}">
                        <i class="eye icon"></i>
                        Show
                    </a>
                    <a class="ui small teal icon button" href="{{ route('roles.edit', [$sid, $role->id]) }}">
                        <i class="pencil icon"></i>
                        Edit
                    </a>
                    {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $sid, $role->id], 'style'=>'display:inline']) !!}
                    <button class="ui small red icon button" type="submit" value="Delete">
                        <i class="remove icon"></i>
                        Delete
                    </button>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </table>
        {!! $roles->render() !!}
    </div>
@endsection
