@extends('layouts.menuapp')

@section('title', 'Show Role')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">Showing Role: {{$role->display_name}}</h2>
                </div>
                <div class="eight wide right aligned column">
                    <a class="ui black labeled icon button" href="{{route('roles.index', $sid)}}">
                        <i class="reply icon"></i> Back
                    </a>
                </div>
            </div>
            <div class="ui row">
                <div class="sixteen wide column">
                    <div class="ui large list">
                        <div class="item">
                            <i class="user circle icon"></i>
                            <div class="content">
                                <div class="header">Role Name</div>
                                <div class="description">{{ $role->display_name }}</div>
                            </div>
                        </div>
                        <div class="item">
                            <i class="comment icon"></i>
                            <div class="content">
                                <div class="header"> Description</div>
                                <div class="description"> {{ $role->description }}</div>
                            </div>
                        </div>
                        <div class="item">
                            <i class="protect icon"></i>
                            <div class="content">
                                <div class="header"> Permissions</div>
                                <!--<div class="description"> These are all the permissions this role has and a description of them</div>-->
                                <div class="list">
                                    @if(!empty($rolePermissions))
                                        @foreach($rolePermissions as $v)
                                            <div class="item">
                                                <i class="angle right icon"></i>
                                                <div class="content">
                                                    <div class="header">{{$v->display_name}}</div>
                                                    <div class="description">{{$v->description}}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
