<h2 class="ui header" style="background-color: #e0e1e2;">
    <span data-maniaplanet data-editable="/server/{{$id}}/ajax/serverinfo" data-string="{!! $server->name !!}">
    {!! Maniaplanet::toHtml($server->name) !!}
    </span>
</h2>

<i class="trophy icon" title="Ladder limit"></i>
{{$server->ladderServerLimitMin / 1000}}k / {{$server->ladderServerLimitMax/1000}}k
<i class="game icon" title="Player count"></i>
{{$playerInfo->players}} / {{$server->currentMaxPlayers}}
<i class="hourglass empty icon" title="Spectator count"></i>
{{$playerInfo->spectators}} / {{$server->currentMaxSpectators}}
