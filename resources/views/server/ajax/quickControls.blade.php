<a class="ui item @if($state == "disabled") disabled @endif" href="{{ route('server.quickcontrols.restartmap', [$id]) }}">
    <i class="undo icon"></i> Restart</a>
<a class="ui item @if($state == "disabled") disabled @endif" href="{{ route('server.quickcontrols.skipmap', [$id]) }}">
    <i class="step forward icon"></i> Skip</a>
<a class="ui item @if($state == "disabled") disabled @endif" href="{{ route('server.quickcontrols.forceendround', [$id]) }}">
    <i class="stop icon"></i> End Round</a>
<a class="ui item @if($state == "disabled") disabled @endif" href="{{ route('server.quickcontrols.cancelvote', [$id]) }}">
   <i class="ban icon"></i> Cancel vote</a>
<script>
    console.log("{{$state}}")
</script>