<?php
/** @var \Maniaplanet\DedicatedServer\Structures\PlayerInfo $player */
?>

    <table class="ui striped table">
        <thead>
        <tr>
            <th class="">#</th>
            <th class="">Status</th>
            <th class="">Login</th>
            <th class="">Nickname</th>
            <th class="">Ladder Score</th>
            <th class="">Actions</th>
        </tr>
        </thead>
        <tbody>

        @foreach($players as $i => $player)
            <?php $login = $player->login ?>

            <tr>
                <td>{{$i+1}}</td>
                <td>@if($player->spectator)
                        <i class="record icon"></i>
                    @else
                        <i class="car icon"></i>
                    @endif
                </td>
                <td>{{ $player->login }}</td>
                <td>{!! Maniaplanet::toBareString($player->nickName) !!}</td>
                <td>{{ round($player->ladderScore, 2) }}</td>
                <td>
                    <div class="ui buttons">
                        {!! Form::open(['route' => ['ajax.handlePlayerListRequest', $id, $player->login]]) !!}
                        @if(in_array($login, $ignoredList))
                        <button class="ui button" type="submit" name="action" value="unmute">Unmute</button>
                        @else
                        <button class="ui button" type="submit" name="action" value="mute">Mute</button>
                        @endif
                        @if(in_array($login, $guestList))
                        <button class="ui button" type="submit" name="action" value="guestlist-remove">Remove as Guest</button>
                        @else
                        <button class="ui button" type="submit" name="action" value="guestlist-add">Add as Guest</button>
                        @endif
                        <button class="ui button" type="submit" name="action" value="kick">Kick</button>
                        <button class="ui button" type="submit" name="action" value="ban">Ban</button>
                        <button class="ui button" type="submit" name="action" value="blacklist">Black</button>
                        <input type="hidden" name="nickname" value="{{$player->nickName}}"></input>
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>


        @endforeach
        </tbody>
    </table>


