<div class="content">
    <div class="center aligned header">{{$name}}</div>
    <div class="meta">Status: {{$status}} </div>
    <div class="description">
        <div>
            <i class="user outline icon"></i> Players: {{$playerCount}} / {{$maxPlayers}}
        </div>
        <div>
            <i class="eye icon"></i> Spectators: {{$specCount}} / {{$maxSpectators}}
        </div>
        <div>
            <i class="map icon"></i> Map: {{$mapName}}
        </div>
    </div>
</div>
<div class="ui three bottom attached buttons">
    <a href="{{$joinLink}}" class="ui button">
        <i class="play icon"></i>
        Join
    </a>
    <a href="{{$specLink}}" class="ui button">
        <i class="eye icon"></i>
        Spectate
    </a>
    <a href="{{route('server.manage', $id)}}" class="ui button">
        <i class="settings icon"></i>
        Manage
    </a>
</div>