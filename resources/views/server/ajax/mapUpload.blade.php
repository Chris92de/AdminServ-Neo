<div class="ui green ribbon label">Upload to Server</div>
<br/>

<form class="dropzone" id="upload" action="/server/{{ $id }}/ajax/mapupload" method="POST">
    {{ Form::token() }}
    <div class="fallback">
        <input name="file" type="file" multiple/>
    </div>
</form>

<link rel="stylesheet" href="{{ URL::asset('/css/dropzone.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('/css/dropzone-custom.css') }}">
<script src="{{ URL::asset('/js/dropzone.min.js')}} "></script>
<script>

    $(function () {
        Dropzone.prototype.defaultOptions.dictDefaultMessage = "<i class='large green cloud upload icon'></i> Click or Drop map file(s) here";
        Dropzone.prototype.defaultOptions.dictFallbackMessage = "Your browser does not support drag'n'drop file uploads.";
        Dropzone.prototype.defaultOptions.dictFallbackText = "Please use the fallback form below to upload your files like in the olden days.";
        Dropzone.prototype.defaultOptions.dictFileTooBig = "File is too big (@{{filesize}}MiB). Max filesize: @{{maxFilesize}}MiB.";
        Dropzone.prototype.defaultOptions.dictInvalidFileType = "You can't upload files of this type.";
        Dropzone.prototype.defaultOptions.dictResponseError = "Server responded with @{{statusCode}} code.";
        Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancel upload";
        Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "Are you sure you want to cancel this upload?";
        Dropzone.prototype.defaultOptions.dictRemoveFile = "Remove file";
        Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "You can not upload any more files.";


        $('#upload').dropzone({
            createImageThumbnails: false,
            success: function (file) {
                this.removeFile(file);
            }

        });
    });
</script>
