<div class="ui two column grid">
    <div class="ui column">
        <div class="ui green ribbon label">Current Map</div>
        <div class="ui basic segment">
            <h3 class="ui header"><span data-maniaplanet>$s{!!  Maniaplanet::toHtml($map->name) !!}</span><span class="ui sub header">{{ $map->author }}
                    ({{ Maniaplanet::toMS($map->authorTime) }})</span></h3>
            {{$map->environnement}} / {{$map->mood}} @if($map->mapStyle) / {{$map->mapStyle }} @endif<br/>
        </div>

    </div>
    <div class="ui column">
        <div class="ui green right ribbon label">Next Map</div>
        <div class="ui right aligned basic segment">

            <h3 class="ui header"><span data-maniaplanet>$s{!!  Maniaplanet::toHtml($map2->name) !!}</span><span class="ui sub header">{{ $map2->author }}
                    ({{ Maniaplanet::toMS($map2->authorTime) }})</span></h3>
            {{$map2->environnement}} / {{$map2->mood}} @if($map2->mapStyle) / {{$map2->mapStyle }} @endif<br/>
        </div>
    </div>

</div>
