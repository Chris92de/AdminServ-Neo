<?php
/** @var \Maniaplanet\DedicatedServer\Structures\Map $map */
?>
<div class="ui green ribbon label">Maplist</div>

<table class="ui celled green table">
    <thead>
    <tr>
        <th>Name</th>
        <th>Author</th>
        <th>Time</th>
        <th>Environment</th>
    </tr>
    </thead>
    <tbody>
    @foreach($maps as $map)
        <tr>
            <td class="active"> {!! Maniaplanet::toHtml('$s'.$map->name) !!}</td>
            <td> {{$map->author}}</td>
            <td> {{ Maniaplanet::toMS($map->goldTime)}}</td>
            <td> {{ $map->environnement }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
