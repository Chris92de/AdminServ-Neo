@extends('layouts.menuapp')

@section('title')
Editing Server #{{$server->id}}
@endsection

@section('content')

    <div class="ui very padded raised container segment">

        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">Edit a Server</h2>
                </div>
                <div class="eight wide column right aligned">
                    <a class="ui black labeled icon button" href="{{ route('server.index') }}"><i
                                class="reply icon"></i><span>Back</span></a>
                </div>
            </div>
        </div>

        {!! Form::model($server, array('route' => ['server.update', $server->id] ,'method'=>'PATCH', "class" => "ui form")) !!}
        <div class="required field {{ $errors->first("name", "error") }}">
            <label class="label">Initial Name</label>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            {!!  $errors->first("name", "<div class='ui pointing red basic label'>:message</div>")  !!}
        </div>
        <div class="field">
            <label class="label">Description</label>
            {!! Form::text('description', null, array('placeholder' => 'Description','class' => 'form-control')) !!}
            {!!  $errors->first("description", "<div class='ui pointing red basic label'>:message</div>")  !!}
        </div>

        <div class="two fields">
            <div class="required field {{ $errors->first("host")  ? "error":"" }} ">
                <label class="label">Host</label>
                {!! Form::text('host', null, array('placeholder' => 'Localhost','class' => 'form-control')) !!}
                {!!  $errors->first("host", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>
            <div class="required field {{ $errors->first("port")  ? "error":"" }}">
                <label class="label">Port</label>
                {!! Form::text('port', null, array('placeholder' => '5000','class' => 'form-control')) !!}
                {!!  $errors->first("port", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>
        </div>
        <div class="two fields">
            <div class="required field {{ $errors->first("user")  ? "error":"" }}">
                <label class="label">User</label>
                {!! Form::text('user', null, array('placeholder' => 'SuperAdmin','class' => 'form-control')) !!}
                {!! $errors->first("user", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>
            <div class="required field {{ $errors->first("pass")  ? "error":"" }}">
                <label class="label">Password</label>
                {!! Form::password('pass', null, array('placeholder' => 'Password','class' => 'form-control')) !!}
                {!!  $errors->first("pass", "<div class='ui pointing red basic label'>:message</div>")  !!}
            </div>
        </div>

        <div class="field">
            <div id="useRemote" class="ui slider checkbox">
                <input type="checkbox" class="hidden"
                       name="useRemote" {{ $server->useRemote ? 'checked' : '' }}/>
                <label for="useRemote">Server uses ftp remote file storage</label>
            </div>
        </div>

        <div id="options" style="display: none;">
            <div class="ui slider checkbox">
                <input type="checkbox" class="hidden"
                       name="useSftp" {{ $server->useSftp ? 'checked' : '' }} />
                <label for="useSftp">Server uses SFTP</label>
            </div>
            <div class="field">
                {!! Form::text('ftpHost', null, array('placeholder' => 'Remote Host')) !!}
                {!! Form::text('ftpPort', null, array('placeholder' => 'Remote Port')) !!}
                {!! Form::text('ftpUser', null, array('placeholder' => 'Remote User')) !!}
                {!! Form::text('ftpPass', null, array('placeholder' => 'Remote Pass')) !!}
            </div>
        </div>

        <div class="field">
            <button type="submit" class="ui green icon labeled button"><i class="checkmark icon"></i>Apply
            </button>
            <a class="ui button" href="{{ route('server.index') }}"><span>Cancel</span></a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@push('scripts')
    <script>
        $(function () {
            $('#useRemote').checkbox({
                onChecked: function () {
                    $('#options').slideDown();
                },
                onUnchecked: function () {
                    $('#options').slideUp();
                }
            });
        });

    </script>
@endpush