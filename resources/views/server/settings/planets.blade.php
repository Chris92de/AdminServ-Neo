@extends('layouts.menuapp')

@section('title', 'Planets')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">Planets</h2>
                </div>
                <div class="eight wide column right aligned">
                    <a class="ui black labeled icon button" href="{{ route('server.manage', [$id]) }}"><i
                                class="reply icon"></i><span>{{__('messages.back')}}</span></a>


                </div>
            </div>
            <div class="row">
                <div class="sixteen wide column">
                    <h4 class="ui header">
                        <i class="globe icon"></i>
                        <div class="content">
                            @php echo trans_choice('messages.server-manage-planets-message', $planets, ['value' => $planets])@endphp
                        </div>
                    </h4>
                    {!! Form::open(["route" => ["server.planets.transfer", $id], "class" => "ui form"]) !!}
                    <h4 class="ui dividing header">{{__('messages.server-manage-planets-transfer')}}</h4>
                    <div class="field">
                        <div class="fields">
                            <div class="four wide field">
                                <label>{{__('messages.select-player')}}</label>
                                <select name="login" class="ui dropdown">
                                    @foreach($playerList as $player)
                                        <option value="{{$player->login}}">{!! Maniaplanet::stripAll($player->nickName) !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="three wide field">
                                <label>{{__('messages.server-manage-planets-amount')}}</label>
                                <input type="number" name="amount">
                            </div>
                            <div class="two wide field">
                                <label>&#0160;</label>
                                <button class="ui green button" type="submit">{{__('messages.transfer')}}</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    {!! Form::open(["route" => ["server.planets.createbill", $id], "class" => "ui form"]) !!}
                    <h4 class="ui dividing header">{{__('messages.server-manage-planets-send-bill')}}</h4>
                    <div class="field">
                        <div class="fields">
                            <div class="four wide field">
                                <label>{{__('messages.select-player')}}</label>
                                <select name="login" class="ui dropdown">
                                    @foreach($playerList as $player)
                                        <option value="{{$player->login}}">{!! Maniaplanet::stripAll($player->nickName) !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="three wide field">
                                <label>{{__('messages.server-manage-planets-amount')}}</label>
                                <input type="number" name="amount">
                            </div>
                            <div class="six wide field">
                                <label>{{__('messages.server-manage-planets-bill-msg')}}</label>
                                <input type="text" name="message">
                            </div>
                            <div class="three wide field">
                                <label>&#0160;</label>
                                <button class="ui yellow button" type="submit">{{__('messages.server-manage-planets-bill-create')}}</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
