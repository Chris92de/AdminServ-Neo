@extends('layouts.menuapp')

@section('title', 'Server Options')

@section('content')
    <div class="ui very padded raised container segment">
        <div class="ui grid">
            <div class="row">
                <div class="eight wide column">
                    <h2 class="ui header">Server Options</h2>
                </div>
                <div class="eight wide column right aligned">
                    <a class="ui black labeled icon button" href="{{ route('server.manage', [$id]) }}">
                        <i class="reply icon"></i>
                        <span>Back</span>
                    </a>
                </div>
            </div>
        </div>

        {!! Form::open(['route' => ["server.options", $id], "class" => 'ui form']) !!}

        @include("partials.genericList", ["data" => $options])

        {{ Form::submit(null, ["class" => "ui green button"]) }}

        {!! Form::close() !!}
    </div>
@endsection













