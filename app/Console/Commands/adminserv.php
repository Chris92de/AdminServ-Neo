<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class adminserv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adminserv:adduser {user} {--email=} {--pass=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an user with given credentials';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $user = User::create(["login" => $this->argument('user'), "name" => $this->argument('user')]);
            $this->info('User created.');
        } catch (\Exception $ex) {
            $this->error("error: " . $ex->getMessage());
            return;
        }

        if ($this->option("pass") || $this->option("email")) {
            if ($user) {
                if ($this->option("pass") && $this->option("email")) {
                    try {
                        $user->password = \Hash::make($this->option("pass"));
                        $user->email = $this->option('email');
                        $this->info('added email and password for the user as well.');
                    } catch (\Exception $ex) {
                        $this->error("error: " . $ex->getMessage());
                    }
                } else {
                    $this->warn("you need to define both email and pass to add it for user!");
                }
            }
        }
    }
}
