<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSetup extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'appurl' => "required",
            'port' => "required|numeric",
            'database' => 'required',
            'user' => 'required',
            'host' => 'required',
            'pass' => 'required'
        ];
    }


    public function messages()
    {
        return [
            // custom error messages:
            // "connection.required" => "test",
        ];
    }

}
