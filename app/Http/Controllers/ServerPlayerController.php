<?php

namespace App\Http\Controllers;

use App\Helpers\Maniaplanet;
use App\Permission;
use App\Server;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Manialib\Formatting\ManiaplanetString;
use Maniaplanet\DedicatedServer\Structures\GameInfos;
use Maniaplanet\DedicatedServer\Structures\ServerOptions;
use Maniaplanet\DedicatedServer\Structures\ScriptSettings;
use Zizaco\Entrust\Entrust;

class ServerPlayerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $id)
    {
        if (!Auth::user()->hasAccessToServer($id) && !Auth::user()->isMasterAdmin())
            return redirect()->route("server.index")->with("error", "You don't have enough permissions to view this page.");
        return view("server.manage.players", compact("id"));
    }

    public function getPlayerlist($id)
    {
        try {
            $players = Maniaplanet::connect($id)->getPlayerList();
            $ignored = Maniaplanet::connect($id)->getIgnoreList();
            $guests = Maniaplanet::connect($id)->getGuestList();
            $ignoredList = array();
            $guestList = array();
            foreach($ignored as $player){
                array_push($ignoredList, $player->login);
            }
            foreach($guests as $player){
                array_push($guestList, $player->login);
            }

            return view("server.ajax.playerlist", compact('players', 'id', 'ignoredList', 'guestList'));
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage() . "<br>Your server might be offline.");
        }

    }

/** Mute, Guestlist, Kick, Ban, Blacklist **/

    public function kickPlayer($id, $login, $nickName){
        try{
            $connection = Maniaplanet::connect($id);
            $connection->kick($login, "Kicked via AdminServ");
            $connection->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$FF0✗ KICK$FFF] ' .$nickName. ' $z$s$fff(Login: '.$login.')' );
        }
        catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage());
        }
    }

    public function banPlayer($id, $login, $nickName)
    {
        try {
            $connection = Maniaplanet::connect($id);
            $connection->ban($login, "Banned via AdminServ");
            $connection->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$F00 BAN$FFF] ' .$nickName. ' $z$s$fff(Login: '.$login.')' );
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage());
        }
    }

    public function blacklistAndBanPlayer($id, $login, $nickName)
    {
        try {
            $connection = Maniaplanet::connect($id);
            $connection->banAndBlackList($login, "Banned via AdminServ");
            $connection->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$000 BLACKLIST$FFF] ' .$nickName. ' $z$s$fff(Login: '.$login.').' );
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage());
        }
    }

    public function addPlayerToGuestlist($id, $login, $nickName)
    {
        try {
            $connection = Maniaplanet::connect($id);
            $connection->addGuest($login);
            $connection->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$7F7 Guestlist.Add$FFF] ' .$nickName. ' $z$s$fff(Login: '.$login.') ' );
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage());
        }
    }

    public function removePlayerFromGuestlist($id, $login, $nickName)
    {
        try{
            $connection = Maniaplanet::connect($id);
            $connection->removeGuest($login);
            $connection->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$F77 Guestlist.Remove$FFF] ' .$nickName. ' $z$s$fff(Login: '.$login.')' );
        } catch (\Exception $ex) {
            return response("Error" . $ex->getMessage());
        }
    }

    public function mutePlayer($id, $login, $nickName)
    {
        try {
            $connection = Maniaplanet::connect($id);
            $connection->ignore($login);
            $connection->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$F77 Mute$FFF] ' .$nickName. ' $z$s$fff(Login: '.$login.')' );
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage());
        }
    }

    public function unmutePlayer($id, $login, $nickName)
    {
        try {
            $connection = Maniaplanet::connect($id);
            $connection->unIgnore($login);
            $connection->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$7F7 Unmute$FFF] ' .$nickName. ' $z$s$fff(Login: '.$login.')' );
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage());
        }
    }

    public function handleRequest(Request $request, $id, $login)
    {
        $nickName = $request->get("nickname");
        switch($request->get("action")){
            case "kick":
                $this->kickPlayer($id, $login, $nickName);
                return redirect()->route("server.manage.players", $id);
                break;
            case "ban":
                $this->banPlayer($id, $login, $nickName);
                return redirect()->route("server.manage.players", $id);
                break;
            case "guestlist-add":
                $this->addPlayerToGuestlist($id, $login, $nickName);
                return redirect()->route("server.manage.players", $id);
                break;
            case "guestlist-remove":
                $this->removePlayerFromGuestlist($id, $login, $nickName);
                return redirect()->route("server.manage.players", $id);
                break;
            case "blacklist":
                $this->blacklistAndBanPlayer($id, $login, $nickName);
                return redirect()->route("server.manage.players", $id);
                break;
            case "mute":
                $this->mutePlayer($id, $login, $nickName);
                return redirect()->route("server.manage.players", $id);
                break;
            case "unmute":
                $this->unmutePlayer($id, $login, $nickName);
                return redirect()->route("server.manage.players", $id);
                break;
            default:
                return redirect()->route("server.manage.players", $id);
                break;
        }
    }
}