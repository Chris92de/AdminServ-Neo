<?php

namespace App\Http\Controllers\Ajax;

use App\Helpers\Maniaplanet;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Manialib\Formatting\ManiaplanetString;
use Maniaplanet\DedicatedServer\Structures\PlayerInfo;

class ServerInfo extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        try {
            $server = Maniaplanet::connect($id)->getServerOptions();
            $formattedServerName = new ManiaplanetString($server->name);
            $server->name = $formattedServerName->stripLinks();
            $playerInfo = $this->getPlayerInfos($id);
            $status = Maniaplanet::connect($id)->getStatus();
            return view("server.ajax.serverInfo", compact('server', 'playerInfo', 'status', 'id'));
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage() . "<br>Your server might be offline.");
        }
    }

    private function getPlayerInfos($id)
    {
        $out = ["spectators" => 0, "players" => 0];
        foreach (Maniaplanet::connect($id)->getPlayerList() as $login => $playerInfo) {
            if ($playerInfo->spectator) {
                $out['spectators']++;
            } else {
                $out['players']++;
            }
        }
        return (object)$out;
    }

    private function getCurrentMap($id){
        try{
            $map = Maniaplanet::connect($id)->getCurrentMapInfo();
            return (object)$map;
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage() ."<br>Server unreachable.");
        }
    }
    public function update(Request $request, $id)
    {
        Maniaplanet::connect($id)->setServerName($request->value);
        return response($request->value);
    }

    public function getPlayerlist($id)
    {
        try {
            $players = Maniaplanet::connect($id)->getPlayerList();
            $ignored = Maniaplanet::connect($id)->getIgnoreList();
            return view("server.ajax.playerlist", compact('players', 'ignored'));
        } catch (\Exception $ex) {
            return response("Error: " . $ex->getMessage() . "<br>Your server might be offline.");
        }

    }

    public function getAllInfo(Request $request, $id){
        try{
            $conn = Maniaplanet::connect($id);
            $playerList = $conn->getPlayerList();
            $playerCount = 0;
            $specCount = 0;
            $maxPlayers = $conn->getMaxPlayers()["CurrentValue"];
            $maxSpectators = $conn->getMaxSpectators()["CurrentValue"];
            foreach($playerList as $player){
                if($player->isSpectator){
                    $specCount++;
                } else {
                    $playerCount++;
                }
            }
            $map = $conn->getCurrentMapInfo();
            $mapName = new ManiaplanetString($map->name);
            $mapName = $mapName->stripAll();
            $name = new ManiaPlanetString($conn->getServerName());
            $name = $name->stripAll();
            $status = $conn->getStatus()->name;
            $login = $conn->getSystemInfo()->serverLogin;
            $joinLink = "maniaplanet://#qjoin=" . $login;
            $specLink = "maniaplanet://#spectate=" . $login;
            return view("server.ajax.widget", compact('name', 'playerCount', 'specCount', 'maxPlayers', 'maxSpectators', 'mapName', 'mapName', 'status', 'joinLink', 'specLink', 'id'));
        } catch (\Exception $ex){
            return response("Error: ". $ex->getMessage() . "<br>Your server might be offline.");
        }
    }
}
