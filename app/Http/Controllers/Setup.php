<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSetup;
use App\User;
use App\Permission;
use App\Role;
use Artisan;
use Auth;
use Exception;
use http\Env;
use Illuminate\Http\Request;
use Hash;

/**
 * Class Setup
 * @package App\Http\Controllers
 */
class Setup extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function start(Request $request)
    {
        return view("setup.mysql");
    }


    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function step1()
    {

        try {
            // Bootstrap artisan
            Artisan::bootstrap();
            Artisan::call('config:cache');
            Artisan::call('migrate:status');
            $info = explode("\r\n", Artisan::output());
            switch ($info[0]) {
                case "No migrations found.":
                    Artisan::call('migrate:install');
                    Artisan::call('migrate');
                    Artisan::call('db:seed');
                    return redirect("/setup/2");
                default:
                    Artisan::call('migrate:refresh');
                    Artisan::call('db:seed');
                    return redirect("/setup/2")->with("info", "success!");
            }
        } catch (\Illuminate\Database\QueryException $queryException) {
            // in case query exception
            if ($queryException->getCode() == "42S01") {
                return redirect("/setup/2")->with("info", "Already migrated");
            }
            return redirect("/setup")->with("error", $queryException->getCode() . "<br/>" . $queryException->getMessage());
        } catch (\PDOException $PDOException) {
            // in case mysql is not connected
            return redirect("/setup")->with("error", $PDOException->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function step2(Request $request)
    {
        return view("setup.maniaplanet");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function step3(Request $request)
    {
        $permission = Permission::get();

        $roles = Role::getAdminserv()->pluck('display_name', 'id');
        return view('setup.createuser', compact('roles', 'permission'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function step4()
    {
        touch(base_path(".setupDone"));
        Artisan::call('config:cache');
        return view("setup.done");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSetupUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'login' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['nickname'] = $request->get("name");

        try {
            $user = User::create($input);

            foreach ($request->input('roles') as $key => $value) {
                $user->attachRole($value);
            }

            Auth::login($user, true);

            return redirect("/setup/4")->with('success', 'User created successfully');
        } catch (Exception $ex) {
            return redirect("/setup/3")->with('error', 'There was error while creating new user.' . $ex->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postSetupOauth(Request $request)
    {
        $ini = $this->parseEnv();
        $ini['USE_OAUTH2'] = $request->get('oauth2', true) ? "true" : "false";
        $ini['MANIAPLANET_CLIENT_ID'] = $request->get('client_id');
        $ini['MANIAPLANET_CLIENT_SECRET'] = $request->get('client_secret');
        try {
            file_put_contents(base_path() . DIRECTORY_SEPARATOR . app()->environmentFile(), $this->makeEnv($ini));
        } catch (Exception $exception) {
            return redirect("/setup/2")->with("error", $exception->getMessage());
        }
        return redirect("setup/3");
    }

    /**
     * @param StoreSetup $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postSetupMySql(StoreSetup $request)
    {
        $ini = $this->parseEnv();
        $ini['CACHE_DRIVER'] = "array";  // needed for entrust roles
        $ini['APP_URL'] = $request->get('appurl');
        // $ini['DB_CONNECTION'] = $request->get('connection', "mysql");
        $ini['DB_CONNECTION'] = "mysql";
        $ini['DB_HOST'] = $request->get('host', "localhost");
        $ini['DB_PORT'] = $request->get('port', "3306");
        $ini['DB_DATABASE'] = $request->get('database', "adminserv");
        $ini['DB_USERNAME'] = $request->get('user', "adminserv");
        $ini['DB_PASSWORD'] = $request->get('pass', "secret");
        try {
            file_put_contents(base_path() . DIRECTORY_SEPARATOR . app()->environmentFile(), $this->makeEnv($ini));
        } catch (Exception $exception) {
            return redirect("/setup")->with("error", $exception->getMessage());
        }
        return redirect("/setup/1");
    }

    /**
     * Parses .env file
     * @return array
     */
    public function parseEnv()
    {
        $ini = file(base_path() . DIRECTORY_SEPARATOR . app()->environmentFile(), FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $array = [];
        foreach ($ini as $line) {
            $i = explode("=", $line, 2);
            $array[$i[0]] = $i[1];
        }
        return $array;
    }

    /**
     * @param array $env
     * @return string $out
     */
    public function makeEnv($env)
    {
        $out = "";
        foreach ($env as $key => $value) {
            $out .= $key . "=" . $value . "\n";
        }
        return $out;
    }

}
