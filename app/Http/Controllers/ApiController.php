<?php


namespace App\Http\Controllers;

use App\Helpers\Maniaplanet;
use App\Server;
use Illuminate\Http\Request;
use Manialib\Formatting\ManiaplanetString;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;

class ApiController extends Controller
{
    public function getServerName(Request $request, $id){
        try{
            $conn = Maniaplanet::connect($id);
            $name = new ManiaPlanetString($conn->getServerName());
            return response()
                ->json(['id'=>$id, 'name'=>$name->stripAll()->toHtml(), 'error'=>null, 'route'=>$request->getPathInfo()]);
        }
        catch (Exception $ex){
            return response()
                ->json(['id'=>$id, 'name'=>null, 'error'=>$ex->getMessage(), 'route'=>$request->getPathInfo()]);
        }
    }

    public function getServerStatus(Request $request, $id){
        try {
            $conn = Maniaplanet::connect($id);
            $status = $conn->getStatus()->name;
            return response()
                ->json(['id'=>$id, 'status'=>$status, 'error'=>null, 'route'=>$request->getPathInfo()]);
        }
        catch(Exception $ex){
            return response()
                ->json(['id'=>$id, 'status'=>null, 'error'=>'Server offline', 'route'=>$request->getPathInfo()]);
        }
    }

    public function getPlayerCount(Request $request, $id){
        try{
            $conn = Maniaplanet::connect($id);
            $playerList = $conn->getPlayerList();
            $count = 0;
            foreach($playerList as $player){
                if(!$player->isSpectator){
                    $count++;
                }
            }
            $maxCount = $conn->getMaxPlayers()["CurrentValue"];
            return response()
                ->json(['id'=>$id, 'count'=>$count.'/'.$maxCount, 'error'=>null, 'route'=>$request->getPathInfo()]);
        } catch (Exception $ex){
            return response()
                ->json(['id'=>$id, 'count'=>null, 'error'=>'Server offline', 'route'=>$request->getPathInfo()]);
        }
    }

    public function getSpectatorCount(Request $request, $id){
        try{
            $conn = Maniaplanet::connect($id);
            $playerList = $conn->getPlayerList();
            $count = 0;
            foreach($playerList as $player){
                if($player->isSpectator){
                    $count++;
                }
            }
            $maxCount = $conn->getMaxSpectators()["CurrentValue"];
            return response()
                ->json(['id'=>$id, 'count'=>$count.'/'.$maxCount, 'error'=>null, 'route'=>$request->getPathInfo()]);
        } catch (Exception $ex){
            return response()
                ->json(['id'=>$id, 'count'=>null, 'error'=>'Server offline', 'route'=>$request->getPathInfo()]);
        }
    }

    public function getCurrentMap(Request $request, $id){
        try{
            $conn = Maniaplanet::connect($id);
            $currentMap = $conn->getCurrentMapInfo();
            $mapName = new ManiaplanetString($currentMap->name);
            return response()
                ->json(['id'=>$id, 'map'=>$mapName->stripAll(). " by " . $currentMap->author, 'error'=>null, 'route'=>$request->getPathInfo()]);
        } catch (Exception $ex){
            return response()
                ->json(['id'=>$id, 'map'=>null, 'error'=>$ex->getMessage(), 'route'=>$request->getPathInfo()]);
        }
    }

    public function getJoinLink(Request $request, $id){
        try{
            $conn = Maniaplanet::connect($id);
            $login = $conn->getSystemInfo()->serverLogin;
            $joinLink = "maniaplanet://#qjoin=" . $login;
            return response()
                ->json(['id'=>$id, 'url'=>$joinLink, 'error'=>null, 'route'=>$request->getPathInfo()]);
        } catch(Exception $ex){
            return response()
                ->json(['id'=>$id, 'url'=>null, 'error'=>$ex->getMessage(), 'route'=>$request->getPathInfo()]);
        }
    }

    public function getSpectatorLink(Request $request, $id){
        try{
            $conn = Maniaplanet::connect($id);
            $login = $conn->getSystemInfo()->serverLogin;
            $joinLink = "maniaplanet://#spectate=" . $login;
            return response()
                ->json(['id'=>$id, 'url'=>$joinLink, 'error'=>null, 'route'=>$request->getPathInfo()]);
        } catch(Exception $ex){
            return response()
                ->json(['id'=>$id, 'url'=>null, 'error'=>$ex->getMessage(), 'route'=>$request->getPathInfo()]);
        }
    }
}