<?php

namespace App\Http\Controllers;


use App\Helpers\Maniaplanet;
use App\Permission;
use App\Server;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maniaplanet\DedicatedServer\Structures\GameInfos;
use Maniaplanet\DedicatedServer\Structures\ServerOptions;
use Maniaplanet\DedicatedServer\Structures\ScriptSettings;
use Zizaco\Entrust\Entrust;


class ServerSettingsController extends Controller
{


    public function serverOptions($id)
    {
        $options = Maniaplanet::connect($id)->getServerOptions();
        return view("server.settings.serverOptions", compact("options", "id"));
    }

    public function postServerOptions(Request $request, $id)
    {
        $new = $request->all();
        unset($new['_token']);
        $options = Maniaplanet::connect($id)->getServerOptions();

        $outOptions = new ServerOptions();
        foreach ($options as $key => $value) {
            if (array_key_exists($key, $new)) {
                settype($new[$key], gettype($value));
                $outOptions->{$key} = $new[$key];
            }
        }

        Maniaplanet::connect($id)->setServerOptions($outOptions);
        return redirect()->route("server.manage", [$id])->with("info", "done!");

    }


    public function gameInfos($id)
    {
        $options = Maniaplanet::connect($id)->getNextGameInfo();
        return view("server.settings.gameInfos", compact("options", "id"));
    }

    public function postGameInfos(Request $request, $id)
    {

        $new = $request->all();
        unset($new['_token']);
        $options = Maniaplanet::connect($id)->getNextGameInfo();

        $outOptions = new GameInfos();
        foreach ($options as $key => $value) {

            if (array_key_exists($key, $new)) {
                settype($new[$key], gettype($value));
                if ($new[$key] === null) $new[$key] = false;
                $outOptions->{$key} = $new[$key];
            }
            if ($outOptions->{$key} === null) $outOptions->{$key} = false;
        }

        Maniaplanet::connect($id)->setGameInfos($outOptions);
        return redirect()->route("server.manage", [$id])->with("info", "done!");

    }

    public function scriptSettings($id) {
        Maniaplanet::connect($id)->triggerModeScriptEvent("XmlRpc.SetApiVersion", ["2.1.0"]);
        $options = Maniaplanet::connect($id)->getModeScriptSettings();
        return view("server.settings.scriptSettings", compact("options", "id"));
    }

    public function postScriptSettings(Request $request, $id)
    {
        $new = $request->all();
        unset($new['_token']);
        $options = Maniaplanet::connect($id)->getModeScriptSettings();

        $outOptions = [];
        foreach($options as $key => $value) {
            if (array_key_exists($key, $new)) {
                settype($new[$key], gettype($value));
                if ($new[$key] === null) $new[$key] = false;
                $outOptions[$key] = $new[$key];
            } else {
                $outOptions[$key] = $value;
            }
            if ($outOptions[$key] === null) $outOptions[$key] = false;
        }

        Maniaplanet::connect($id)->setModeScriptSettings($outOptions);
        return redirect()->route("server.manage", [$id])->with("info", "Done!");
    }
}
