<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Manialib\Formatting\ManiaplanetString;
use App\Server;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $username = new ManiaplanetString(Auth::user()->name);
        if (Auth::user()->isMasterAdmin()) {
            $servers = Server::on();
        } else {
            $servers = Auth::user()->servers();
        }
        $perPage = 30;
        $servers = $servers->orderBy('id', 'ASC')->paginate($perPage);
        return view('home', compact('username', 'servers'))
            ->with('i', ($request->input('page', 1) - 1) * $perPage);

    }
}
