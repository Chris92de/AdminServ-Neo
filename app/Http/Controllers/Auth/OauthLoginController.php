<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use Manialib\Formatting\ManiaplanetString;

class OauthLoginController extends Controller
{
    protected $redirectTo = "/home";

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('maniaplanet')->scopes(["basic", "dedicated"])->redirect();
    }

    /**
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        $user = Socialite::with('maniaplanet')->stateless()->user();

        $authUser = User::where('login', $user->user['login'])->first();

        if ($authUser) {
            /** @var ManiaplanetString $name */
            $name = new ManiaPlanetString($user->user['nickname']);
            $authUser->name = $name->stripAll();
            $authUser->nickname = $user->user['nickname'];
            $authUser->save();
            Auth::login($authUser, true);
            return redirect($this->redirectTo)->with('success', "Logged in as " . $authUser->name);
        } else {
            return redirect()->route('users.index')->with('error', "this maniaplanet login is not our records.");
        }
    }

    public
    function findOrCreateUser($user)
    {
        $authUser = User::where('login', $user->user['login'])->first();
        /** @var ManiaplanetString $name */
        $name = new ManiaPlanetString($user->user['nickname']);

        if ($authUser) {
            $authUser->name = $name->stripAll();
            $authUser->nickname = $user->user['nickname'];
            $authUser->save();
            return $authUser;
        }

        return User::create([
            'name' => $name->stripAll(),
            'login' => $user->user['login'],
            'nickname' => $user->user['nickname'],
            'path' => $user->user['path']
        ]);
    }
}
