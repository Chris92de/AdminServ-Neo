<?php

namespace App\Http\Controllers;


use App\Helpers\Maniaplanet;
use App\Permission;
use App\Server;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Maniaplanet\DedicatedServer\Structures\GameInfos;
use Maniaplanet\DedicatedServer\Structures\ServerOptions;
use Manialib\Gbx\Map;
use Zizaco\Entrust\Entrust;


class ManiaExchangeController extends Controller
{


    public function index(Request $request, $id)
    {

        $mapsData = Maniaplanet::connect($id)->getMapList();

        $maps = $this->paginate($mapsData, 15);

        return view("server.maps.upload", compact('maps', "id"))
            ->with('i', ($request->input('page', 1) - 1) * 50);

    }

    public function paginate($items, $perpage)
    {
        $total = count($items);
        $currentpage = \Request::get('page', 1);
        $offset = ($currentpage * $perpage) - $perpage;
        $itemstoshow = array_slice($items, $offset, $perpage);
        $p = new LengthAwarePaginator($itemstoshow, $total, $perpage);
        $p->setPath(\Request::getBasePath());
        return $p;
    }


    public function postInstall(Request $request, $id)
    {
        $connection = Maniaplanet::connect($id);
        $this->validate($request, [
            "mxid" => "required|numeric",
            "gametype" => "required"
        ]);
        $mxId = trim($request->get("mxid"));

        $query = "";
        switch ($request->get('gametype')) {
            case "SM":
                $query = 'https://sm.mania-exchange.com/tracks/download/' . $mxId;
                break;
            case "TM":
                $query = 'https://tm.mania-exchange.com/tracks/download/' . $mxId;
                break;
        }


        $options = array(CURLOPT_HTTPHEADER => array());


        $guzzle = new Client([
            'timeout' => 5,
            'allow_redirects' => true,
        ]);

        $response = $guzzle->request('GET', $query, ["X-ManiaPlanet-ServerLogin" => $connection->getSystemInfo()->serverLogin]);

        if ($response->getStatusCode() == 200) {
            // $mapdir = $connection->getMapsDirectory();
            $file = $mxId . ".Map.Gbx";
            $string = $response->getBody()->getContents();

            Maniaplanet::fileAccess($id)->put('/UserData/Maps/' . $file, $string);

            $map = Map::loadString($string);

            //if ($connection->checkMapForCurrentServerParams($file)) {
            try {
                //Maniaplanet::connect($id)->writeFile($file, base64_encode($string));
                //$custom_callback_data = array('filename' => $file,
                    //'map_uid' => $map->getUid(),
                    //'map_author' => $map->getAuthor(),
                    //'added_via' => 'mx'); // options here will be local_file, file_upload and mx
                //$custom_callback_data = json_encode(array('map' => $custom_callback_data), JSON_FORCE_OBJECT);

                //$connection->dedicatedEcho('AdminServ.Map.Added', $custom_callback_data);
                $connection->addMap($file);
            } catch (\Exception $ex) {
                return back()->with(["warning" => "upload to server failed: " . $ex->getMessage()]);
            }
            Maniaplanet::connect($id)->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$7F7 Maps.Add $FFF(M$08FX$FFF)] ' . $map->getName() . ' $z$s$FFFby ' . $map->getAuthor());
            return back()->with(["info" => "success"]);
            //} else {
            //     return back()->with(["warning" => "current map type is not supported."]);
        }
        return back()->with(["warning" => "mania-exchange http error:" . $response->getStatusCode()]);
    }

}
