<?php

namespace App\Http\Controllers;

use App\Helpers\Maniaplanet;
use Illuminate\Http\Request;
use Manialib\Gbx\Map;

class FileBrowserController extends Controller
{
    public function index($id, Request $request)
    {
        $conn = Maniaplanet::connect($id);

        $path = $request->get("path", ".");
        $path = ltrim(str_replace("..", "", $path), "/");

        $files = Maniaplanet::fileAccess($id)->listContents("UserData/Maps/".$path);
        array_multisort(array_column($files, 'type'), SORT_ASC, SORT_NATURAL, $files);
        $dirs = [];
        $maps = [];

        foreach ($files as $file) {

            switch ($file['type']) {
                case "dir":
                    $dirs[] = $file['basename'];
                    break;
                case "file":
                    $ext = strtolower($file['extension']);
                    if ($ext == "gbx") {
                        try {
                            $maps[] = Map::loadString(Maniaplanet::fileAccess($id)->read($file['path']));
                        } catch (\Exception $e) {

                        }
                        break;
                    }
            }
        }

        return view("server.maps.filebrowser", compact('id', 'dirs', 'maps', 'path'));
    }
}