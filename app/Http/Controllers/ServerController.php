<?php

namespace App\Http\Controllers;


use App\Helpers\Maniaplanet;
use App\Permission;
use App\Server;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\Entrust;


class ServerController extends Controller
{

    public function index(Request $request)
    {
        if (Auth::user()->isMasterAdmin()) {
            $servers = Server::on();
        } else {
            $servers = Auth::user()->servers();
        }

        $perPage = 15;
        $servers = $servers->orderBy('id', 'ASC')->paginate($perPage);
        return view('server.index', compact('servers'))
            ->with('i', ($request->input('page', 1) - 1) * $perPage);

    }

    public function edit(Request $request, $id)
    {
        if (!Auth::user()->hasAccessToServer($id) && !Auth::user()->isMasterAdmin())
            return redirect()->route("server.index")->with("error", "you don't have enough permissions to access this server");

        /** @var Server $server */
        $server = Server::find($id);
        $users = $server->users()->get();

        return view("server.edit", compact("server", "users"));

    }

    public function manage(Request $request, $id)
    {
        if (!Auth::user()->hasAccessToServer($id) && !Auth::user()->isMasterAdmin())
            return redirect()->route("server.index")->with("error", "you don't have enough permissions to access this server");
        return view("server.manage", compact("id"));
    }


    public function delete(Request $request, $id)
    {
        if (!Auth::user()->hasAccessToServer($id) && !Auth::user()->isMasterAdmin())
            return redirect()->route("server.index")->with("error", "you don't have enough permissions to access this server");

        Server::find($id)->delete();
        return redirect()->route('server.index')
            ->with('success', 'Server deleted successfully');
    }


    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasAccessToServer($id) && !Auth::user()->isMasterAdmin())
            return redirect()->route("server.index")->with("error", "you don't have enough permissions to access this server");

        $this->validate($request, [
            'name' => 'required',
            'host' => 'required',
            'port' => 'required | numeric',
            'user' => "required",
            'pass' => "required"
        ]);

        /** @var Server $server */
        $server = Server::find($id);


        $options = $request->all();
        $options['useRemote'] = $request->get("useRemote") ? true : false;
        $options['useSftp'] = $request->get("useSftp") ? true : false;
        $server->update($options);
        $server->save();

        return redirect()->route('server.index')->with("info", "success!");
    }


    public function create(Request $request)
    {
        return view("server.create");
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'host' => 'required',
            'port' => 'required | numeric',
            'user' => "required",
            'pass' => "required"
        ]);

        $options = $request->all();
        $options['useRemote'] = $request->get("useRemote") ? true : false;
        $options['useSftp'] = $request->get("useSftp") ? true : false;
        Server::create($options);
        return redirect()->route('server.index')->with("info", "success!");
    }

    //private $server = Server::
}
