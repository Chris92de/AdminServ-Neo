<?php

namespace App\Http\Controllers;

use App\Helpers\Maniaplanet;
use App\Permission;
use App\Server;
use App\User;
use Illuminate\Http\Request;
use Maniaplanet\DedicatedServer\Xmlrpc\Exception;

class Planets extends Controller {

    public function overview($id){

        $planets = Maniaplanet::connect($id)->getServerPlanets();
        $playerList = Maniaplanet::connect($id)->getplayerList();
        return view("server.settings.planets", compact("planets", "playerList", "id"));

    }

    public function transfer($id, Request $request){
        $login = $request->get("login");
        $player = Maniaplanet::connect($id)->getPlayerInfo($login);
        $amount = (int) $request->get("amount");
        $planets = Maniaplanet::connect($id)->getServerPlanets();
        if($amount > Maniaplanet::connect($id)->getServerPlanets() - 500){
            return back()->with(["error" => "The amount you're trying to transfer is higher than the server's current planets."]);
        } else {
            try{
                Maniaplanet::connect($id)->pay($login, $amount, "Transferred via \$fffAdmin\$0cfServ");
            } catch (\Exception $ex){
                return back()->with(["error" => $ex->getMessage()]);
            }
            Maniaplanet::connect($id)->chatSendServerMessage('$z$s$fff#Admin$0cfServ $fff» [$0FF Planets.Payout$FFF] '.$amount.' planets → '.$player->nickName.' $z$s$FFF(Login: '.$player->login.')');
            return back()->with(["success" => "Planets successfully transferred"]);
        }
    }

    public function billPlayer($id, Request $request){
        $login = $request->get("login");
        $amount = (int) $request->get("amount");
        $message = $request->get("message");
        try{
            Maniaplanet::connect($id)->sendBill($login, $amount, $message);
        } catch (\Exception $ex){
            return back()->with(["error" => $ex->getMessage()]);
        }
        return back()->with(["success" => "Bill sent to player."]);
    }
}