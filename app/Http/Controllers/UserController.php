<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;

use App\User;
use App\Role;
use DB;
use Hash;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id', 'ASC')->paginate(5);
        return view('admin.users.index', compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::getAdminserv()->pluck('display_name', 'id');
        $permission = Permission::get();
        $lang = $this->getAvailableLanguages();
        return view('admin.users.create', compact('roles', 'permission', 'lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'login' => 'required|unique:users,login',
            'name' => 'required|unique:users,name',
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input['password'] = null;
        }

        if (!empty($input['email'])) {
            $this->validate($request, [
                'email' => 'unique:users,email',
            ]);
        }

        if (empty($input['name'])) {
            $input['name'] = $input['login'];
        }

        $user = User::create($input);
        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('users.index')
            ->with('success', 'User created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $roles = Role::getAdminserv()->pluck('display_name', 'id');
        return view('admin.users.show', compact('user', 'role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::getAdminserv()->pluck('display_name', 'id');
        $userRole = $user->roles->pluck('id', 'id')->toArray();
        $lang = $this->getAvailableLanguages();

        return view('admin.users.edit', compact('user', 'roles', 'userRole', 'lang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = array_except($input, array('password'));
        }


        /** @var User $user */
        $user = User::find($id);
        $user->update($input);
        DB::table('role_user')->where('user_id', $id)->delete();

        foreach ($request->input('roles') as $key => $value) {
            $user->attachRole($value);
        }

        return redirect()->route('users.index')
            ->with('success', 'User updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
            ->with('success', 'User deleted successfully');
    }

    public function getAvailableLanguages()
    {
        $lang = [];
        foreach (new \DirectoryIterator(resource_path('lang')) as $file) {
            if ($file->isDir() && !$file->isDot()) {
                $lang[] = $file->getFilename();
            }
        }
        return $lang;
    }

    public function assignRole(Request $request, $sid, $uid) {
        $user = User::find($uid);
        $role = Role::find($request->get('role'));
        $user->attachRole($role);
        return redirect()->route('users.index')
            ->with('success', 'Assigned role to user successfully');
    }
}


