<?php
/**
 * Created by PhpStorm.
 * User: Käyttäjä
 * Date: 6.5.2017
 * Time: 14:56
 */

namespace App\Observers;

use App\Role;

class ServerObserver
{

    public function created(\App\Server $server)
    {
        $masterAdmin = ['name' => 'masteradmin', 'display_name' => 'Master Admin', 'description' => 'Master admin of ' . $server->name, "server_id" => $server->id];
        /** @var \App\Role $role */
        $role = \App\Role::create($masterAdmin);

        $permissions = [];
        foreach(\App\Permission::get() as $permission) {
            if(substr($permission->name, 0, 7) != 'global-'){
                array_push($permissions, $permission);
            }
        }

        $role->attachPermissions($permissions);
        $role->save();

        /** @var \App\Role $role */
        $admin = \App\Role::create(['name' => 'admin', 'display_name' => 'Admin', 'description' => 'Admin of ' . $server->name, "server_id" => $server->id]);
        $operator = \App\Role::create(['name' => 'operator', 'display_name' => 'Operator', 'description' => 'Operator of ' . $server->name, "server_id" => $server->id]);
    }

    public function deleted(\App\Server $server)
    {
        $roles = Role::on()->where("server_id", "=", $server->id)->get();
        /** @var Role $role */
        foreach ($roles as $role) {
            $role->delete();
        }
    }

}
