<?php

namespace App\Helpers;

use Laravel\Socialite\SocialiteManager;

class Socialite extends SocialiteManager
{
    public function createManiaplanetDriver()
    {
        $config = $this->app['config']['services.maniaplanet'];
        return $this->buildProvider(ManiaplanetProvider::class, $config);
    }
}
