<?php

namespace App\Helpers;

use App\Server;
use League\Flysystem\Adapter\Ftp as FtpAdapter;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Adapter\NullAdapter;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;
use Maniaplanet\DedicatedServer\Connection;

class Maniaplanet
{

    /*
     * @return string */
    public static function toHtml($string)
    {
        $out = new \Manialib\Formatting\ManiaplanetString($string);

        return $out->stripLinks()->toHtml();
    }

    public static function toTM($time)
    {
        return self::MStoTM($time);
    }

    public static function toMS($stamp)
    {
        return self::fromTM($stamp);
    }

    public static function toBareString($string)
    {
        $out = new \Manialib\Formatting\ManiaplanetString($string);

        return $out->stripAll()->toHtml();
    }

    public static function stripAll($string)
    {
        $out = new \Manialib\Formatting\ManiaplanetString($string);

        return $out->stripAll();
    }


    /**
     * connect to Server
     * @param integer|Server $id
     * @return Connection
     */
    public static function connect($id)
    {
        if ($id instanceof \App\Server) {
            $id = $id->id;
        }

        $server = \App\Server::findOrFail($id);
        $connection = \Maniaplanet\DedicatedServer\Connection::factory($server->host, $server->port, 3, $server->user,
            $server->pass);
        $connection->triggerModeScriptEvent("XmlRpc.SetApiVersion", ["2.1.0"]);

        return $connection;
    }

    /**
     * Waits 15sec for required callback and returns the value or throws error on timeout
     * @param Connection $connection
     * @param string     $callback
     * @return mixed
     */
    public static function returnCallback(Connection $connection, $callback)
    {
        $connection->enableCallbacks(true, true);
        $connection->triggerModeScriptEvent("XmlRpc.EnableCallbacks", ["true"], true);
        $connection->triggerModeScriptEvent("XmlRpc.SetApiVersion", ["2.1.0"], true);
        $connection->executeMulticall();

        $startTime = time();
        while (true) {
            foreach ($connection->executeCallbacks() as $cbdata) {
                // $cbdata[0] string <- callback name
                // $cbdata[1] array <- callback data
                if ($cbdata[0] == $callback) {
                    if (empty($cbdata[1])) {
                        return true;
                    }

                    return $cbdata[1];
                }
                if ($cbdata[0] == "ManiaPlanet.ModeScriptCallbackArray") {
                    if ($cbdata[1][0] == $callback) {
                        return json_decode($cbdata[1][0]);
                    }
                }
            }

            if (time() > $startTime + 15) {
                return false;
            }
            usleep(100);
        }
    }

    /**
     * Server FileAccess
     * @param $id serverid
     * @return Filesystem
     */
    static public function fileAccess($id)
    {
        $server = \App\Server::findOrFail($id);
        try {
            $dir = dirname(self::connect($id)->gameDataDirectory());

            if ($server->useRemote) {
                $options = [
                    'host' => $server->ftpHost,
                    'port' => $server->ftpPort,
                    'username' => $server->ftpUser,
                    'password' => $server->ftpPass,
                    'root' => $dir,
                    'timeout' => 20,
                ];
                if ($server->useSftp) {
                    return new Filesystem(new SftpAdapter($options));
                } else {
                    $options['passive'] = true;
                    $options['ssl'] = false;

                    return new Filesystem(new FtpAdapter($options));
                }
            } else {
                return new Filesystem(new Local($dir));
            }
        } catch (\Exception $ex) {
            return new Filesystem(new NullAdapter());
        }
    }


    public static function MStoTM($string)
    {
        $timelimit = explode(":", trim($string));
        if (count($timelimit) == 1) {
            return intval($timelimit[0] * 1000);
        } else {
            return intval($timelimit[0] * 60 * 1000) + intval($timelimit[1] * 1000);
        }
    }

    public static function TMtoMS($time)
    {
        $time = intval($time);

        return gmdate("i:s", $time / 1000);
    }

    static function fromTM($timestamp, $signed = false)
    {
        $time = (int)$timestamp;
        $negative = ($time < 0);
        if ($negative) {
            $time = abs($time);
        }

        $cent = str_pad(($time % 1000), 3, '0', STR_PAD_LEFT);
        $time = floor($time / 1000);
        $sec = str_pad($time % 60, 2, '0', STR_PAD_LEFT);
        $min = str_pad(floor($time / 60), 1, '0');
        $time = $min.':'.$sec.'.'.$cent;

        if ($signed) {
            return ($negative ? '-'.$time : '+'.$time);
        } else {
            return $time;
        }

    }

}
