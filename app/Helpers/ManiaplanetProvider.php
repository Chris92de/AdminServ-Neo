<?php

namespace App\Helpers;


use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

class ManiaplanetProvider extends AbstractProvider implements ProviderInterface
{

    const SCOPE_BASIC = 'basic';
    const SCOPE_DEDICATED = 'dedicated';
    const SCOPE_TITLES = 'titles';
    const SCOPE_EVENTS = 'events';
    const SCOPE_MAPS = 'maps';

    /**
     * {@inheritdoc}
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase('https://v4.live.maniaplanet.com/login/oauth2/authorize', $state);
    }

    /*protected function usesState()
    {
        return true;
    }*/

    protected function getCodeFields($state = null)
    {
        $fields = [
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUrl,
            'scope' => $this->formatScopes($this->getScopes(), $this->scopeSeparator),
            'response_type' => 'code',
        ];

        if ($this->usesState()) {
            $fields['state'] = $state;
        }

        return array_merge($fields, $this->parameters);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl()
    {
        return 'https://v4.live.maniaplanet.com/login/oauth2/access_token';
    }

    /**
     * {@inheritdoc}
     */
    public function getAccessToken($code)
    {
        $response = $this->getHttpClient()->post($this->getTokenUrl(), [
            'code' => $this->getTokenFields($code),
            'grant_type' => "authorization_code",
            'client_id' => env('MANIAPLANET_CLIENT_ID'),
            'client_secret' => env('MANIAPLANET_CLIENT_SECRET'),
            'redirection_uri' => $this->redirectUrl
        ]);
        return $this->parseAccessToken($response->getBody());
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenFields($code)
    {
        return array_add(
            parent::getTokenFields($code), 'grant_type', 'authorization_code'
        );
    }

    /**
     * {@inheritdoc}
     */


    protected function getUserByToken($token)
    {
        $response = $this->getHttpClient()->get('https://www.maniaplanet.com/webservices/me', [
            'headers' => [
                'Authorization' => 'Bearer ' . trim($token),
            ],
        ]);
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * {@inheritdoc}
     */
    protected function formatScopes(array $scopes, $scopeSeparator)
    {
        return implode(' ', $scopes);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user)
    {
        $new = new User();
        $new->setRaw($user)->map(
            [
                "nickname" => $user['nickname'],
                "name" => $user['login'],
            ]);
        return $new;
    }
}
