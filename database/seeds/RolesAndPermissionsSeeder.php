<?php 
/*
|--------------------------------------------------------------------------
| Roles & Permissions Seeder
|--------------------------------------------------------------------------
|
| This Seeder class allows you to update and create Roles & Permissions 
| for the Laravel Entrust package. 
|
| USE -> php artisan db:seed --class=RolesAndPermissionsSeeder
| 
| https://github.com/thomasfw/RolesAndPermissionsSeeder
|
|--------------------------------------------------------------------------
| Make sure you update the namespaces for your User & Entrust models
|--------------------------------------------------------------------------
*/
use \App\User as User;
use \App\Role as Role;
use \App\Permission as Permission;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class RolesAndPermissionsSeeder extends Seeder {


	protected $roles = [
	
		'adminserv-master'  => [
		    'display_name'  => 'Global Admin',
            'description'   => 'Has access to everything',
            'permissions'   => ['global-server-create','global-server-delete','global-server-edit','global-user-create', 'global-user-edit', 'global-user-delete','role-list','role-create','role-edit','role-delete','role-assign','chat','maps-add','map-delete','map-mx','map-playlist','player-kick','player-ban','player-blacklist','player-moveteam','game-control','game-settings','server-options','server-passwords','planets-view','planets-send'],
            'server_id'     => '-1'
        ],
        'adminserv-server-owner'  => [
            'display_name'  => 'Server Owner',
            'description'   => 'Has full permissions for his servers',
            'permissions'   => ['global-server-create','global-server-edit','global-server-delete','role-list','role-create','role-edit','role-delete','role-assign','chat','maps-add','map-delete','map-mx','map-playlist','player-kick','player-ban','player-blacklist','player-moveteam','game-control','game-settings','server-options','server-passwords','planets-view','planets-send'],
            'server_id'     => '-1'
        ],
        'adminserv-user'    => [
            'display_name'  => 'User',
            'description'   => 'Basic user, only has permissions granted by Global Admin or Server Owner',
            'server_id'     => '-1'
        ]
	];
	
	
	protected $permissions = [
        'global-server-create' => [
            'display_name' => '(GLOBAL) Create new servers',
            'description' => '(GLOBAL) Allows user to create new servers'
        ],
        'global-server-edit' => [
            'display_name' => '(GLOBAL) Edit existing servers',
            'description' => '(GLOBAL) Allows user to edit server'
        ],
        'global-server-delete' => [
            'display_name' => '(GLOBAL) Delete existing servers',
            'description' => '(GLOBAL) Allows user to delete existing servers'
        ],
        'global-user-create' => [
            'display_name' => '(GLOBAL) Create new user accounts',
            'description' => '(GLOBAL) Allows user to create new user accounts'
        ],
        'global-user-edit' => [
            'display_name' => '(GLOBAL) Edit user accounts',
            'description' => '(GLOBAL) Allows user to edit existing user accounts'
        ],
        'global-user-delete' => [
            'display_name' => '(GLOBAL) Delete existing user accounts',
            'description' => '(GLOBAL) Allows user to delete existing user accounts'
        ],
        'role-list' => [
            'display_name' => 'Display Role Listing',
            'description' => 'Access Server Role Overview'
        ],
        'role-create' => [
            'display_name' => 'Create Server Role',
            'description' => 'Allows user to create new server roles'
        ],
        'role-edit' => [
            'display_name' => 'Edit Server Role',
            'description' => 'Allows user to modify server roles'
        ],
        'role-delete' => [
            'display_name' => 'Delete Server Role',
            'description' => 'Allows user to delete server roles'
        ],
        'role-assign' => [
            'display_name' => '(Un-)Assign server roles to users',
            'description' => 'Allows user to assign server roles to users'
        ],
        'chat' => [
            'display_name' => 'Access to chat messages',
            'description' => 'Allows user to see and send server chat messages'
        ],
        'maps-add' => [
            'display_name' => 'Add a map',
            'description' => 'Gives user permission to add a map'
        ],
        'map-delete' => [
            'display_name' => 'Delete a map',
            'description' => 'Gives user permission to delete maps'
        ],
        'map-mx' => [
            'display_name' => 'Add map from MX',
            'description' => 'Gives user permission to add a map from MX'
        ],
        'map-playlist' => [
            'display_name' => 'Modify playlists',
            'description' => 'Gives user permission to modify playlists'
        ],
        'player-kick' => [
            'display_name' => 'Kick a player',
            'description' => 'Gives user permission to kick a player'
        ],
        'player-ban' => [
            'display_name' => 'Ban a player',
            'description' => 'Gives user permission to ban a player'
        ],
        'player-blacklist' => [
            'display_name' => 'Blacklist a player',
            'description' => 'Gives user permission to blacklist a player'
        ],
        'player-moveteam' => [
            'display_name' => 'Change team of player',
            'description' => 'Gives user permission to change a players team'
        ],
        'game-control' => [
            'display_name' => 'Control the current game',
            'description' => 'Allows user to Skip/Previous/Restart Map/End Round'
        ],
        'game-settings' => [
            'display_name' => 'Game settings',
            'description' => 'Allows user to modify match and script settings'
        ],
        'server-options' => [
            'display_name' => 'Server settings',
            'description' => 'Allows user to modify current server options'
        ],
        'server-passwords' => [
            'display_name' => 'Server passwords',
            'description' => 'Allows user to modify current server join/spec passwords'
        ],
        'planets-view' => [
            'display_name' => 'View planets',
            'description' => 'Allows user to view server planets'
        ],
        'planets-send' => [
            'display_name' => 'Send planets',
            'description' => 'Allows user to send planets to player accounts'
        ]
	];
	
    
	 /**
	* Roles
	*
	* @return array()
	*/
	public function roles() 
	{    	
		return $this->roles;
	}
    
	/**
	* Permissions
	*
	* @param  $name
	* @return array()
	*/
	public function permissions($name = '') 
	{
		$single = (array_key_exists($name,$this->permissions) ? array($name =>$this->permissions[$name]) : false );
		return ($name ? $single : $this->permissions);
	}
    
    
	/**
	* Run the Seeder
	*
	* @return void
	*/
	public function run()
	{	
		DB::table(Config::get('entrust.permissions_table'))->delete();
		
		foreach ($this->roles() as $key => $val) {    
			$this->command->info(" ");
			$this->command->info('Creating/updating the \''.$key.'\' role');
			$this->command->info('-----------------------------------------');
			$val['name'] = $key;
			$this->reset($val);
		}
		$this->cleanup();
	}
	
	
	/**
	* Reset Role, Permissions & Users
	*
	* @param  $role
	* @return void
	*/
	public function reset($role) 
	{
		$commandBullet = '  -> ';
		
		// The Old Role
		$originalRole = Role::where('name',$role['name'])->first();
		if($originalRole) Role::where('id',$originalRole->id)->update(['name' => $role['name'].'__remove']);
		    	
		// The New Role
		$newRole = new Role();
		$newRole->name  = $role['name'];
		if(isset($role['display_name'])) $newRole->display_name  = $role['display_name']; // optional
		if(isset($role['description'])) $newRole->description  = $role['description']; // optional
        if(isset($role['server_id'])) $newRole->server_id = $role['server_id']; // optional
		$newRole->save();
		$this->command->info($commandBullet."Created $role[name] role");
		
		// Set the Permissions (if they exist)
		$pcount = 0;
		if(!empty($role['permissions']))
		{
	    	foreach ($role['permissions'] as $permission_name) {
	    		
	    		$permission = $this->permissions($permission_name);
	    		if($permission === false || (!$permission_name)) {
					$this->command->error($commandBullet."Failed to attach permission '$permission_name'. It does not exist");
	    			continue;
	    		}
	    		
				$newPermission = Permission::where('name',$permission_name)->first();
				if (!$newPermission) {
					$newPermission = new Permission();
					$newPermission->name = key($permission);
					if(isset($permission[key($permission)]["display_name"])) $newPermission->display_name = $permission[key($permission)]["display_name"]; // optional
					if(isset($permission[key($permission)]["description"])) $newPermission->description  = $permission[key($permission)]["description"]; // optional
					$newPermission->save();
				}	    		
	    		$newRole->attachPermission($newPermission);
	    		$pcount++;
	    	}
		}
		$this->command->info($commandBullet."Attached $pcount permissions to $role[name] role");
		
		// Update old records  
		if ($originalRole) 
		{  
			$userCount = 0;
			$RoleUsers = DB::table(Config::get('entrust.role_user_table'))->where('role_id',$originalRole->id)->get();
			foreach ($RoleUsers as $user) {
				$u = User::where('id',$user->user_id)->first();
				$u->attachRole($newRole);
				$userCount++;
			}
			$this->command->info($commandBullet."Updated role attachment for $userCount users");
			
			Role::where('id',$originalRole->id)->delete(); // will also remove old role_user records
			$this->command->info($commandBullet."Removed the original $role[name] role");
		}
	}
	
	
	/**
	* Cleanup()
	* Remove any roles & permissions that have been removed
	* @return void
	*/
	public function cleanup() 
	{
		$commandBullet = '  -> ';
		$this->command->info(" ");
		$this->command->info('Cleaning up roles & permissions:');
		$this->command->info('--------------------------------');
		
		$storedRoles = Role::all();
		if(!empty($storedRoles)) {
	    	$definedRoles = $this->roles();
	    	foreach ($storedRoles as $role) {
	    		if ( !array_key_exists($role->name,$definedRoles) ) {
	    			Role::where('name',$role->name)->delete();    			
	    			$this->command->info($commandBullet.'The \''.$role->name.'\' role was removed');
	    		}
	    	}
	    }
		$storedPerms = DB::table(Config::get('entrust.permissions_table'))->get();
		if(!empty($storedPerms)) {
	    	$definedPerms = $this->permissions();
	    	foreach ($storedPerms as $perm) {
	    		if ( !array_key_exists($perm->name,$definedPerms) ) {
	    			DB::table(Config::get('entrust.permissions_table'))->where('name',$perm->name)->delete();
	    			$this->command->info($commandBullet.'The \''.$perm->name.'\' permission was removed');
	    		}
	    	}
	    }
	    $this->command->info($commandBullet.'Done');
		$this->command->info(" ");
	}
	
}